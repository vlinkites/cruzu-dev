var express = require('express'),
    config = require('./config/config'),
    http = require('http'),
    db = require('./lib/db-config'),
    passport = require('passport'),
    mongoStore = require('connect-mongo')(express),
    request = require('request'),
    userModel = require('./models/user'),
    monthArray = ["January","February","March","April","May","June","July","August","September","October","November","December"];

/**
 * Process level exception catcher
 */


process.on('uncaughtException', function (err) {
    console.log("Node NOT Exiting...");
    console.log(err);
    console.log(err.stack)
});


var allowCrossDomain = function(req, res, next) {
    res.header('Access-Control-Allow-Origin', '*');
    res.header('Access-Control-Allow-Methods', 'GET,PUT,POST,DELETE');
    res.header('Access-Control-Allow-Headers', 'Content-Type,Authorization');

    // intercept OPTIONS method
    if ('OPTIONS' == req.method) {
        res.send(200);
    }
    else {
        next();
    }
}

var app = express();

db.connectDatabase(function(db){

    /**
     * Passport.js intiallization
     */
    require('./lib/passport-config')(passport);

    /**
     * Express Configuration
     */
    app.configure(function() {
        app.use(express.logger('dev'));
        app.use(express.compress());
        app.use(express.methodOverride());
        app.use(express.cookieParser());
        app.use(express.bodyParser({keepExtensions: true, uploadDir:__dirname+'/uploads'}));
        app.use(express.session({
            store:new mongoStore({url: config.get('db:mongodb:url')}),
            secret: 'secret'
        }));
        app.use(allowCrossDomain);
        app.use(passport.initialize());
        app.use(passport.session());
        app.use(express.static(__dirname + '/public'));
        app.set('views', __dirname + '/public');
        app.engine('html', require('ejs').renderFile);
    });

    app.configure('development', function() {
        app.use(express.errorHandler({
            dumpExceptions: true,
            showStack: true
        }));
    });

    app.configure('production', function() {
        app.use(express.errorHandler());
    });

    configureControllers(app);

    app.get('/recentoffer/user/:userid/offer/:offerid', function (req, res){
        var userid = req.param("userid");
        var offerid = req.param("offerid");

        userModel.getUserByCondition({_id: userid}, function (err, existingUser) {
            if (err || existingUser == null) {
                res.redirect('/error.html');
            }
            else {
                // req.logIn(existingUser, function (err) {
                //     if (err) {
                //         return next(err);
                //     }
                //     console.log(userid);
                // });

                if(offerid && userid){
                    request("http://162.222.227.147:5000/user/"+userid+"/offer/"+offerid, function (error, response, body) {
                      if (!error && response.statusCode == 200) {
                        var dateObj = new Date();
                        res.render('offer.ejs', {offer:JSON.parse(body), user:existingUser, dateString:monthArray[dateObj.getMonth()] + " " + dateObj.getDate() + ", " + dateObj.getFullYear()});
                      } else {
                        res.redirect('/error.html');
                      }
                    }); 
                } else {
                    res.redirect("/error.html");
                }

            }
        });
    });

    app.get('*', function(req, res){
        res.redirect('/error.html');
    });

    // Setup Express error handler middleware!
    app.use(function(err, req, res, next){
        res.send(err.code,{error:err.toString()});
    });

    /**
     * Start http server here
     */
    var server = http.createServer(app);
    server.listen(process.env.PORT ? process.env.PORT : config.get('main:port'));
    console.log("Listening on " + config.get('main:port'));
//    require('./lib/utility-functions').insertBoard(function(){
//        console.log("Finish");
//    })
});

function configureControllers(app) {
    [
        'authentication','user','offer', 'product', 'board', 'venue', 'app', 'question', 'query'
    ].map(function(controllerName) {
            var controller = require('./controllers/' + controllerName);
            return controller.setup(app);
        });
}