var FacebookStrategy = require('passport-facebook').Strategy,
    BearerStrategy = require('passport-http-bearer').Strategy,
    config = require('../config/config'),
    userModel = require('../models/user'),
    customError = require('../lib/custom_errors'),
    utility = require('../lib/utility-functions'),
    _ = require('underscore')._;

module.exports = function (passport) {
    passport.serializeUser(function (user, done) {
        done(null, user);
    });

    passport.deserializeUser(function (obj, done) {
        done(null, obj);
    });

    passport.use(new FacebookStrategy(config.get('apps:facebook'),
        function (accessToken, refreshToken, profile, done) {
            // asynchronous verification, for effect...
            process.nextTick(function () {
                var arr = profile._json.location ? profile._json.location.name.split(',') : [];
                var user = {
                    fb_id: profile._json.id,
                    fb_access_token: accessToken,
                    name: profile._json.name,
                    email: profile._json.email,
                    firstName: profile._json.first_name,
                    lastName: profile._json.last_name,
                    image: 'http://graph.facebook.com/' + profile._json.id + '/picture',
                    address: {
                        city: arr.length > 0 ? arr[0] : "",
                        state_region: arr.length > 1 ? (getShortState(arr[1].trim()) ? getShortState(arr[1].trim()) : "CA") : "CA"
                        // We're going to default to California for now
                    }
                };
                return done(null, user);
            });
        }
    ));

    passport.use(new BearerStrategy({ "passReqToCallback": true },
        function(req, token, done) {
            // asynchronous validation, for effect...
            process.nextTick(function () {
                userModel.getUserByCondition({api_key:token},function(err,user){
                    if (err) { return done(err); }

                    if(user==null) { return done(new customError.NotAuthorized("Wrong key provided.")); }

                    req["userId"] = user._id.toString();
                    return done(null, user);
                })
            });
        }
    ));
};

// Return true if param is a valid US state
function getShortState(fbState) {
    fbState = fbState.toLowerCase();
    var states = utility.USStateList();
    states.forEach(function (state) {
        if (state.name.toLowerCase() == fbState)
            return state.abbreviation;
    });
    return null;
}
