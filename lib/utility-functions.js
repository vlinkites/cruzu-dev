var fs = require('fs');
var path = require('path'),
    async = require('async'),
    questionModel = require('../models/question'),
    boardModel = require('../models/board'),
    userModel = require('../models/user'),
    reviewerModel = require('../models/reviewer'),
    sendgrid = require('sendgrid')("cruzu", "wineisawesome"),
    vintageModel = require('../models/vintage'),
    customError = require('./custom_errors'),
    productModel = require('../models/product'),
    offerModel = require('../models/offer_history');

module.exports.isValidObjectId = function(id){
    var checkForHexRegExp = new RegExp("^[0-9a-fA-F]{24}$");
    return checkForHexRegExp.test(id);
}

module.exports.ensureAuthenticated = function(req, res, next) {
    if (req.isAuthenticated()) {
        return next();
    }

    return next(new customError.NotAuthorized("You are not authorized"))
}

module.exports.verifyParameters = function(requestBody, keys){
    var message = "";
    var isAllFieldExists = true;
    for(var i=0;i<keys.length;i++){
        if(!requestBody[keys[i]]){
            isAllFieldExists = false;
            message = "Some parameters missing. Required parameters are "+keys.toString();
            break;
        }
    }

    if(!isAllFieldExists)
        return {isError:true,message:message};

    var isAllFieldsFilled = true;
    for(var i=0;i<keys.length;i++){
        if(requestBody[keys[i]].length==0){
            message = requestBody[keys[i]]+" can not be empty.";
            isAllFieldsFilled = false;
            break;
        }
    }

    if(!isAllFieldsFilled)
        return {isError:true,message:message};

    return {isError:false,message:""};
}

module.exports.insertQuestion = function(questionType,callback){
    var questions = [
        {
            "type" : questionType,
            "text" : "Let's say you bring a bottle of wine to a dinner party, would you prefer that the host...",
            "answer" : {
                "datatype" : "string",
                "sort_order" : 1,
                "control_type" : "radiobutton",
                "option_list" : [{
                    "display_value" : "immediately recognizes the well-known, respected wine you bring",
                    "return_value" : "BRAND_KNOWN"
                }, {
                    "display_value" : "asks \"what is this\" and requires you to tell the story about the wine",
                    "return_value" : "BRAND_UNKNOWN"
                }, {
                    "display_value" : "You don't really care what the host thinks",
                    "return_value" : "BRAND_UNKNOWN"
                }]
            }
        },
        {
            "type" : questionType,
            "text" : "Birth Year",
            "answer" : {
                "datatype" : "number",
                "sort_order" : 2,
                "control_type" : "range",
                "range" : {
                    "low" : 1913,
                    "high" : 2013,
                    "step" : 1
                }
            }
        },
        {
            "type" : questionType,
            "text" : "Vintage",
            "answer" : {
                "datatype" : "string",
                "sort_order" : 3,
                "control_type" : "text"
            }
        },
        {
            "type" : questionType,
            "text" : "Let's say you bring a bottle of wine to a dinner party, would you prefer that the host...",
            "answer" : {
                "datatype" : "string",
                "sort_order" : 4,
                "control_type" : "dropdown",
                "option_list" : [{
                    "display_value" : "immediately recognizes the well-known, respected wine you bring",
                    "return_value" : "BRAND_KNOWN"
                }, {
                    "display_value" : "asks \"what is this\" and requires you to tell the story about the wine",
                    "return_value" : "BRAND_UNKNOWN"
                }, {
                    "display_value" : "You don't really care what the host thinks",
                    "return_value" : "BRAND_UNKNOWN"
                }]
            }
        },
        {
            "type" : questionType,
            "text" : "Let's say you bring a bottle of wine to a dinner party, would you prefer that the host...",
            "answer" : {
                "datatype" : "string",
                "sort_order" : 5,
                "control_type" : "checkbox",
                "option_list" : [{
                    "display_value" : "immediately recognizes the well-known, respected wine you bring",
                    "return_value" : "BRAND_KNOWN"
                }, {
                    "display_value" : "asks \"what is this\" and requires you to tell the story about the wine",
                    "return_value" : "BRAND_UNKNOWN"
                }, {
                    "display_value" : "You don't really care what the host thinks",
                    "return_value" : "BRAND_UNKNOWN"
                }]
            }
        }
    ];

    async.forEach(questions,function(question,cb){
        questionModel.addQuestion(question,function(err,result){
            cb();
        })
    },function(err){
        callback();
    })
}

module.exports.insertBoard = function(callback){
    var board = {
        "name" : "Cabs of the week",
        "owner_id" : "5248ec32a85f3ee005000005",
        "description" : "These are great cabs",
        "confirmation" : "Thanks for following me!",
        "publish_state" : "published",
        "questions" : ["5228bb69868c03381a000001", "5228bb69868c03381a000005", "5228bb69868c03381a000002","5228bb69868c03381a000004","5228bb69868c03381a000003"],
        "products" : []
    };

    boardModel.addBoard(board,function(err,result){
        callback();
    })
}

module.exports.insertInfluencer = function(callback){
    var user = {
        "address" : {
        "city" : "",
            "country" : ""
    },
        "category" : "Individual",
        "email" : "kq@hotmail.com",
        "fb_access_token" : "CAAIjqQD9cQ0BABAFf2qZC7xa2dQZAZCZBY53bGZA0FbupAgpx3GsyK2i3pPUcKziuEycCYSu8exLccC2804gLFDmyV3nOUGgPK7nPlCzlBJ2FZBwGlFK9sjZAEsA3lRLyZCYRrzjR2ecUd3N9ymaXzkki5MFVH3RZBYv0rRYVJheASGxKOZBZA0Hlts1wQYw91arFMZD",
        "fb_id" : "747730470",
        "followers" : [],
        "following" : [],
        "image" : "http://graph.facebook.com/747730470/picture",
        "is_featured" : false,
        "is_influencer" : true,
        "name" : "Danish Khan",
        "profile_answers" : []
    }

    userModel.registerUser(user,function(err,user){
        callback();
    })
}

module.exports.cloneObject=function clone(obj){

    if(obj == null || typeof(obj) != 'object')
        return obj;

    var temp = new obj.constructor(); // changed (twice)
    for(var key in obj)
        temp[key] = clone(obj[key]);

    return temp;
}


module.exports.USStateList = function() {

    return [
        	        {name:"Alabama", label:"Alabama",abbreviation:"AL"},
        	        {name:"Alaska", label:"Alaska",abbreviation:"AK"},
        	        {name:"Arizona", label:"Arizona",abbreviation:"AZ"},
        	        {name:"Arkansas", label:"Arkansas",abbreviation:"AR"},
        	        {name:"California", label:"California",abbreviation:"CA"},
        	        {name:"Colorado", label:"Colorado",abbreviation:"CO"},
        	        {name:"Connecticut", label:"Connecticut",abbreviation:"CT"},
        	        {name:"Delaware", label:"Delaware",abbreviation:"DE"},
        	        {name:"District of Columbia", label:"District of Columbia",abbreviation:"DC"},
        	        {name:"Florida", label:"Florida",abbreviation:"FL"},
        	        {name:"Georgia", label:"Georgia",abbreviation:"GA"},
        	        {name:"Hawaii", label:"Hawaii",abbreviation:"HI"},
        	        {name:"Idaho", label:"Idaho",abbreviation:"ID"},
        	        {name:"Illinois", label:"Illinois",abbreviation:"IL"},
        	        {name:"Indiana", label:"Indiana",abbreviation:"IN"},
        	        {name:"Iowa", label:"Iowa",abbreviation:"IA"},
        	        {name:"Kansas", label:"Kansas",abbreviation:"KS"},
        	        {name:"Kentucky", label:"Kentucky",abbreviation:"KY"},
        	        {name:"Louisiana", label:"Louisiana",abbreviation:"LA"},
        	        {name:"Maine", label:"Maine",abbreviation:"ME"},
        	        {name:"Maryland", label:"Maryland",abbreviation:"MD"},
        	        {name:"Massachusetts", label:"Massachusetts",abbreviation:"MA"},
        	        {name:"Michigan", label:"Michigan",abbreviation:"MI"},
        	        {name:"Minnesota", label:"Minnesota",abbreviation:"MN"},
        	        {name:"Mississippi", label:"Mississippi",abbreviation:"MS"},
        	        {name:"Missouri", label:"Missouri",abbreviation:"MO"},
        	        {name:"Montana", label:"Montana",abbreviation:"MT"},
        	        {name:"Nebraska", label:"Nebraska",abbreviation:"NE"},
        	        {name:"Nevada", label:"Nevada",abbreviation:"NV"},
        	        {name:"New Hampshire", label:"New Hampshire",abbreviation:"NH"},
        	        {name:"New Jersey", label:"New Jersey",abbreviation:"NJ"},
        	        {name:"New Mexico", label:"New Mexico",abbreviation:"NM"},
        	        {name:"New York", label:"New York",abbreviation:"NY"},
        	        {name:"North Carolina", label:"North Carolina",abbreviation:"NC"},
        	        {name:"North Dakota", label:"North Dakota",abbreviation:"ND"},
        	        {name:"Ohio", label:"Ohio",abbreviation:"OH"},
        	        {name:"Oklahoma", label:"Oklahoma",abbreviation:"OK"},
        	        {name:"Oregon", label:"Oregon",abbreviation:"OR"},
        	        {name:"Pennsylvania", label:"Pennsylvania",abbreviation:"PA"},
        	        {name:"Rhode Island", label:"Rhode Island",abbreviation:"RI"},
        	        {name:"South Carolina", label:"South Carolina",abbreviation:"SC"},
        	        {name:"South Dakota", label:"South Dakota",abbreviation:"SD"},
        	        {name:"Tennessee", label:"Tennessee",abbreviation:"TN"},
        	        {name:"Texas", label:"Texas",abbreviation:"TX"},
        	        {name:"Utah", label:"Utah",abbreviation:"UT"},
        	        {name:"Vermont", label:"Vermont",abbreviation:"VT"},
        	        {name:"Virginia", label:"Virginia",abbreviation:"VA"},
        	        {name:"Washington", label:"Washington",abbreviation:"WA"},
        	        {name:"West Virginia", label:"West Virginia",abbreviation:"WV"},
        	        {name:"Wisconsin", label:"Wisconsin",abbreviation:"WI"},
        	        {name:"Wyoming", label:"Wyoming",abbreviation:"WY"}
        	  ]

};


// SEND AN EMAIL
module.exports.sendEmail = function (to, toName, from, fromName, subject, content, cb) {

    if (!to || !subject)
        return cb(-1);

    var Email = sendgrid.Email;
    var email = new Email({
        to: to,
        toname: toName,
        from: from,
        fromname: fromName,
        subject: subject,
        html: content
    });

    sendgrid.send(email, function (err, json) {
        if (err) {
            console.error(err);
        }
        return cb(err);
    });
};