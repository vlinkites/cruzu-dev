var customError = require('../lib/custom_errors'),
    venueModel = require('../models/venue'),
    appModel = require('../models/app'),
    boardModel = require('../models/board'),
    sourceBoardModel = require('../models/source_board_stats'),
    userModel = require('../models/user'),
    async = require('async'),
    config = require('../config/config'),
    request = require('request'),
    ObjectId = require('mongoose').Types.ObjectId,
    passport = require('passport'),
    utilityFunction = require('../lib/utility-functions'),
    _ = require('underscore')._,
    util = require('../lib/utility-functions');

exports.setup = function (app) {
    app.get('/venue', passport.authenticate('bearer', { session: false }), getNearByVenues);
    app.get('/venue/:id', passport.authenticate('bearer', { session: false }), getVenueDetails);
    app.post('/agent/:id/board/:board_id/items', passport.authenticate('bearer', { session: false }), getProducts);

    // Web routeshttp.+?\/wine-\d{3,6}(-\d{4}-)http.+?\/wine-\d{3,6}(-\d{4}-)
    app.post('/web/agent/:id/items', utilityFunction.ensureAuthenticated, getProducts);
    app.get('/web/venue/all', utilityFunction.ensureAuthenticated, getAllVenues);
};


function getAllVenues(req, res, next) {
    venueModel.find({}, '_id name type location image image_large has_beacons trigger_beacons', {limit: req.query.count}, function (err, venues) {
        if (err)
            return next(err);

        res.send(venues);
    })
}

function getNearByVenues(req, res, next) {
    if (!req.query.near || !req.query.count)
        return next(new customError.MissingParameter("Required parameter are missing."));

    var temp = req.query.near.split(',');
    if (temp.length < 1 || isNaN(temp[0].trim()) || isNaN(temp[1].trim()) || isNaN(req.query.count))
        return next(new customError.InvalidArgument("Coordinates or count is not in correct format."));

    var cord = [parseFloat(temp[0].trim()), parseFloat(temp[1].trim())];
    venueModel.find({location: { $nearSphere: cord, $maxDistance: 100000}, active: {$ne: false}, disable: {$ne:true} }
        , '_id name type location image image_large city has_beacons trigger_beacons price_string', {limit: req.query.count}, function (err, venues) {
        if (err)
            return next(err);

        res.send(venues);
    })
}

function getVenueDetails(req, res, next) {
    if (!req.params.id)
        return next(new customError.MissingParameter("Required parameter are missing."));

    venueModel.findOne({_id: req.params.id}, function (err, venue) {
        if (err)
            return next(err);
        if (venue == null)
            return next(new customError.InvalidArgument("Venue does not exist."));

        // Determine which apps we want to retrieve based on our venue
        var pred = {};
        var venueObj = venue.toObject();
        if (venueObj.list && venueObj.list.is_list_type)
            pred = {$or : [{target_venue_type: {$in:["Restaurant","All Sources"]}},{target_venues:venueObj._id}]};
        else
            pred = {$or : [{target_venue_type: {$in:["Retail","All Sources"]}},{target_venues:venueObj._id}]};
        pred.app_deleted = {$ne:true};

        appModel.
            find(pred)
            .sort('sort_order')
            .populate('pages.block_list.agent', '_id name short_bio long_bio image')
            .exec(function (err, apps) {
                if (err)
                    return next(err);

                var toReturn = venue.toObject();
                var populatedApps = [];
                async.eachLimit(apps, 1, function (app, cb) {
                    var temp = app.toObject();
                    temp.source_id = toReturn._id;                  // So we don't have to change app code
                    modifyAppWithLibraryContent(temp, venue, function (err, temp) {

                        async.forEach(temp.pages, function (page, pageCb) {
                            async.forEach(page.block_list, function (block, blockCb) {
                                if (block.agent) {
                                    boardModel.getBoard({"_id": block.board}, function (err, board) {

                                        if (board) {    // board could have been deleted since app was created
                                            var boardTemp = board.toObject();

                                            /*
                                            for (var i = 0; i < boardTemp.questions.length; i++) {
                                                var answer = _.find(block.selected_values, function (selectedValue) {
                                                    return selectedValue.code.toLowerCase() == boardTemp.questions[i].code.toLowerCase();
                                                });
                                                if (answer) {
                                                    boardTemp.questions[i].selected_values = {
                                                        // Return all return_value and display_value to app as arrays - even for single value fields
                                                        return_value: (typeof answer.return_value == "string") ? [answer.return_value] : answer.return_value,
                                                        display_value: (typeof answer.display_value == "string") ? [answer.display_value] : answer.display_value,
                                                        image: answer.image
                                                    }
                                                }
                                            }
                                            */

                                            block.agent.agent_alias = boardTemp.agent_alias;
                                            block.agent.long_bio = boardTemp.agent_alias_bio ? boardTemp.agent_alias_bio : block.agent.long_bio;
                                            block.agent.long_note = boardTemp.long_note;
                                            block.agent.short_note = boardTemp.short_note;
                                            block.agent.board_image = boardTemp.image;

                                            // We now just need some of the predicate content, so return the parts that matter
                                            block.agent.predicate = boardTemp.sections[0].predicate;
                                        }
                                        delete block.selected_values;
                                        block.agent["questions"] = boardTemp ? boardTemp.questions : [];

                                        // Get block stats for block
                                        sourceBoardModel.findOne({source_id:venueObj._id, board_id:block.board}, function(err,stats) {
                                            block.stats = stats ? stats.toObject() : null;
                                            blockCb();
                                        });
                                    })
                                }
                                else {
                                    blockCb();
                                    delete block.selected_values;
                                }
                            }, function (err) {

                                // Now make sure we have content for block, sort them, personalize, etc.
                                // Only include those blocks that have count > 0 in them
                                page.block_list = _.filter(page.block_list, function(block) {return block.stats && block.stats.count>0});
                                // Now sort our blocks based on block stats. In future we will personalize this
                               // page.block_list = _.sortBy(page.block_list, function(block) {return -block.stats.sort_score});
                                pageCb();
                            })
                        }, function (err) {
                            populatedApps.push(temp);
                            cb();
                        })
                    });
                }, function (err) {
                    toReturn["apps"] = populatedApps;
                    res.send(toReturn);
                })
            })
    })
}

// Insert dynamic content into app by substituting special LIBRARY block with image blocks that link to pages.
// Initially we will only do this for one block in an app (usually on the home page)
function modifyAppWithLibraryContent(app, venue, cb) {
    venue = venue.toObject();
    // This is a hack to identify the block that we will be replacing (if it exists in app.page)
    var linkBoardID = ObjectId("52ab4f119619ec2faac18e02");
    var libraryAppID = ObjectId("53a24692792eee14504d7203");
    var venuePageNames = venue.library_items;
    var blockToReplace;                                             // This is what we're going to replace with new content
    var blockToReplacePage;
    var newBlocks = [];
    var newPages = [];

    async.forEach(app.pages, function (page, pageCb) {
            async.forEach(page.block_list, function (block, blockCb) {

                // See if this is the special hacky to-replace block
                if (block.type == "image" && newBlocks.length == 0 && block.item_list && block.item_list.length > 0 &&
                    block.item_list[0].link_board && block.item_list[0].link_board.toString() == linkBoardID.toString()) {

                    blockToReplace = block;
                    blockToReplacePage = page;

                    // Load the library app that we will pluck pages out of
                    appModel.findOne({_id: libraryAppID})
                        .populate('pages.block_list.agent', '_id source_id name short_bio long_bio image')
                        .exec(function (err, app) {
                            if (err)
                                return cb(-1, app);
                            app = app.toObject();

                            var libraryPages = app.pages;
                            for (var i = 0; i < venuePageNames.length; i++) {
                                var foundPage = _.find(libraryPages, function (libraryPage) {
                                    return (libraryPage.title == venuePageNames[i]);
                                });
                                // If we find the page (we should always unless data gets mixed up), then insert it into the current page
                                // and add a new page to the app
                                if (foundPage) {
                                    newPages.push(foundPage);
                                    var newBlock = {};
                                    newBlock.type = "image";
                                    newBlock.item_size = "large";
                                    newBlock.spotlight = true;
                                    newBlock.title = foundPage.title;
                                    newBlock.image = foundPage.image;
                                    newBlock.item_list = [
                                        {link_type: "page", link_code: foundPage.page_code}
                                    ];
                                    newBlocks.push(newBlock);
                                }
                            }
                            blockCb();
                        });
                }
                else {
                    blockCb();
                }
            }, function () {
                pageCb();
            })
        },
        function () {
            // now add pages and blocks
            if (newPages.length > 0)
                app.pages = app.pages.concat(newPages);

            // Go through blocks in original page, remove the replace block and insert other blocks if we have them
            // Only process if we actually have a block to replace
            if (blockToReplace) {
                for (var i = 0; i < blockToReplacePage.block_list.length; i++) {
                    if (blockToReplace._id.toString() == blockToReplacePage.block_list[i]._id.toString()) {
                        // Found the block
                        if (newBlocks.length > 0)
                            blockToReplacePage.block_list = blockToReplacePage.block_list.slice(0, i)
                                .concat(newBlocks).concat(blockToReplacePage.block_list.slice(i + 1));
                        else
                            blockToReplacePage.block_list = blockToReplacePage.block_list.slice(0, i)
                                .concat(blockToReplacePage.block_list.slice(i + 1));
                        break;
                    }
                }
            }

            cb(null, app);
        });
}


function getProducts(req, res, next) {

    // Both source_id and offer_count are optional parameters
    // We get source_id, not venue_id, so no need to look up source_id. Just pass it on
    var agentParam = "agent_id=" + req.params.id + "&";
    var boardParam = "board_id=" + req.params.board_id + "&";
    var sourceParam = req.query.source ? "source=" + req.query.source + "&" : "";
    var countParam = req.query.count ? "count=" + req.query.count : "";
    var showNoScoreParam =  "&show_noscore="+req.query.show_noscore;

    request.post(config.get('queryURLBase') + "/offers?" + agentParam + boardParam + sourceParam + countParam + showNoScoreParam,
        {
            json: req.body
        }, function (err, response, body) {
            if (err)
                return next(new customError.HttpRequestRejected("Failed to get data"));

            res.send(body);
        })
}