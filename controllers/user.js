var config = require('../config/config'),
    userModel = require('../models/user'),
    boardModel = require('../models/board'),
    questionModel = require('../models/question'),
    supportModel = require('../models/support'),
    request = require('request'),
    offerModel = require('../models/offer_history'),
    utilityFunction = require('../lib/utility-functions'),
    customError = require('../lib/custom_errors'),
    async = require('async'),
    ObjectId = require('mongoose').Types.ObjectId,
    sendgrid = require('sendgrid')("cruzu", "wineisawesome"),       // This is required for sendFeedback()
     MailChimpAPI = require('mailchimp').MailChimpAPI,
    _ = require('underscore'),
    path           = require('path'),
    templatesDir   = path.join(__dirname, '../public/templets'),
    emailTemplates = require('email-templates'),
    fs = require("fs"),
    nodemailer     = require('nodemailer');

exports.setup = function (app) 
{
    app.get('/user/influencers', utilityFunction.ensureAuthenticated, getInfluencers);
    app.get('/user/authenticated', utilityFunction.ensureAuthenticated, getAuthenticatedUser);
    app.get('/user/:user_id/offer', getOffer);
    app.get('/user/:user_id/offer/:offer_id', getOffers);
    app.get('/user/next_offer', utilityFunction.ensureAuthenticated, getSingleOffer);
    app.get('/user/:id/offer/:offer_id', utilityFunction.ensureAuthenticated, getOfferDetails);
    app.get('/user/offer/:offer_id/source/:source_id/save_click', saveClick);
    app.put('/user/:id/follow', utilityFunction.ensureAuthenticated, followUser);
    app.post('/user/save_click', utilityFunction.ensureAuthenticated, saveClick);
    app.post('/user/follow_user_board', utilityFunction.ensureAuthenticated, followUserWithBoard);
    app.post('/user/update_profile', utilityFunction.ensureAuthenticated, updateProfile);
    app.get('/user/:id/board', utilityFunction.ensureAuthenticated, getUserBoard);
    app.get('/user/general_questions', utilityFunction.ensureAuthenticated, askGeneralQuestion);
    app.post('/user/general_questions', utilityFunction.ensureAuthenticated, saveGeneralQuestion);
    app.post('/user/:offer_id/send_feedback', utilityFunction.ensureAuthenticated, sendFeedback);

    app.get('/user/:id/preferences', getUserPreferences);
    app.post('/user/:id/preferences', setUserPreferences);
    app.post('/beta/request_beta_access', addToMailingList);

    // email route

    app.post('/user/:user_id/offer/:offer_id', emailLatestOffer);
    app.post('/user/:user_id/offer', emailLatestOffer);
    app.get('/user/:user_id/latestoffer', emailLatestOffer);
};

function oauth(req, res, next) {
    var options = {};
    if (req.params.service_name.toLowerCase() == "facebook")
        options = { scope: ['email'] };
    passport.authenticate(req.params.service_name, options)(req, res, next);
}

function getInfluencers(req, res, next) {

    //   console.log(JSON.stringify(config,null,4));
    console.log(JSON.stringify(config.stores.file.store.host.hostName));

    userModel.getUsersByCondition({'is_influencer': true}, function (err, users) {
        if (err)
            return next(err);

        var result = [];
        for (var i = 0; i < users.length; i++) {
            var temp = {
                id: users[i]._id,
                name: users[i].name,
                image: users[i].image,
                short_bio: users[i].short_bio,
                long_bio: users[i].long_bio,
                category: users[i].category,
                priority: users[i].priority,
                follower_count: users[i].followers.length,
                is_follower: false,
                in_development: users[i].in_development,
                is_featured: users[i].is_featured,
                has_influencer_questions: users[i].has_influencer_questions,
                tags: users[i].tags,
                title: "FOLLOW"
            };
            for (var k = 0; k < users[i].followers.length; k++) {
                var follower = users[i].followers[k];
                if (follower == req.user._id) {
                    temp.is_follower = true;
                    temp.title = "FOLLOWING"

                    break;
                }
            }
            result.push(temp);
        }
        res.send(result);
    })
}

function followUser(req, res, next) {
    if (!utilityFunction.isValidObjectId(req.params.id))
        return next(new customError.InvalidArgument("Influencer id is not in correct format."));

    userModel.getUserByCondition({_id: req.params.id}, function (err, user) {
        if (err)
            return next(err);

        boardModel.getBoard({owner_id: user._id}, function (err, board) {
            if (err)
                return next(err);

            var isFollower = false;
            for (var i = 0; i < user.followers.length; i++) {
                if (req.user._id.toString() == user.followers[i].toString())
                    isFollower = true;
            }

            if (!isFollower) {
                if (board && board.questions.length > 0)
                    return res.send({status: "ask question"});

                async.series([
                    function (callback) {
                        userModel.followUser(req.params.id, req.user._id.toString(), "followers", function (err, result) {
                            if (err)
                                return callback(err);

                            callback();
                        })
                    },
                    function (callback) {
                        userModel.followUser(req.user._id.toString(), req.params.id, "following", function (err, result) {
                            if (err)
                                return callback(err);

                            callback();
                        })
                    }
                ], function (err, result) {
                    res.send({status: "You are now following."})
                })
            }
            else {
                async.series([
                    function (callback) {
                        userModel.unFollowUser(req.params.id, req.user._id.toString(), "followers", function (err, result) {
                            if (err)
                                return callback(err);

                            callback();
                        })
                    },
                    function (callback) {
                        userModel.unFollowUser(req.user._id.toString(), req.params.id, "following", function (err, result) {
                            if (err)
                                return callback(err);

                            callback();
                        })
                    },
                    function (callback) {
                        userModel.updateUserBoardAnswers(req.user._id.toString(), {board_answers: {influencer_id: ObjectId(req.params.id)}}, function (err, result) {
                            if (err)
                                return callback(err);

                            callback();
                        })
                    }
                ], function (err, result) {
                    res.send({status: "You are now not following."})
                })
            }
        })
    })
}

function getUserBoard(req, res, next) {
    if (!utilityFunction.isValidObjectId(req.params.id))
        return next(new customError.InvalidArgument("Influencer id is not in correct format."));

    boardModel.getBoard({owner_id: ObjectId(req.params.id)}, function (err, board) {
        if (err || !board)
            return next(err);

        var result = {};
        result = {
            id: board._id,
            name: board.name,
            description: board.description,
            image: board.owner_id.image,
            confirmation: board.confirmation
        }

        var controls = [];
        userModel.getUserByCondition({_id:req.user._id}, function(err, user){
            if (err || !user)
                return next(err);


            var answers = _.find(user.board_answers, function(board_answer){
                return board_answer.board_id.toString() == board._id.toString();
            });

            if (answers && answers.length > 0)
                answers = answers.answers;
            else
                answers = user.profile_answers;

            for (var i = 0; i < board.questions.length; i++) {
                var control = getControl(board.questions[i]);
                var temp = _.find(answers, function(answer){
                    return answer.id.toString() == control.id.toString();
                })
                control["value"] = temp ? temp.value : "";
                control["return_value"] = temp ? temp.return_value : "";
                controls.push(control);
            }

            result["controls"] = controls;
            res.send(result);
        })
    })
}

function followUserWithBoard(req, res, next) {
    try {
        var data = req.body instanceof Object ? req.body : JSON.parse(req.body);
        var boardAnswers = {};
        boardAnswers["board_id"] = ObjectId(data.board.id);
        boardAnswers["influencer_id"] = ObjectId(data.influencer_id);
        var answers = [];
        for (var i = 0; i < data.board.controls.length; i++) {
            var answer = data.board.controls[i];
            if (answer.type == "checkbox") {
                var val = [];           // Display value array
                var returnVal = [];     // Return value array
                for (var j = 0; j < answer.options.length; j++) {
                    var option = answer.options[j];
                    if (option.checked) {
                        val.push(option.value);
                        returnVal.push(option.return_value);
                    }
                }
                answers.push({
                    id: ObjectId(answer.id),
                    value: val,
                    return_value: returnVal,
                    code: answer.code
                })
            }
            else {
                answers.push({
                    id: ObjectId(answer.id),
                    value: (answer.answer) ? answer.answer.value : "",
                    return_value: (answer.answer) ? answer.answer.return_value : "",
                    code: answer.code
                })
            }
        }
        boardAnswers["answers"] = answers;
        userModel.getUserByCondition({_id: req.user._id}, function (err, user) {
            if (err)
                return next(err);

            async.series([
                function (callback) {

                    // Only add to "followers" if they're not already in the followers list. This occurs when user "personalizes"
                    // the answers of an influencer they've already followed. So check the user.following array and only if the
                    // influencer is NOT in there should we add to user.following and influencer.followers
                    var isAlreadyFollowing = _.some(user.following, function (following) {
                        return following.toString() == data.influencer_id.toString();
                    });
                    if (isAlreadyFollowing)
                        callback();
                    else {
                        userModel.followUser(data.influencer_id, req.user._id.toString(), "followers", function (err, result) {
                            if (err) return callback(err);
                            userModel.followUser(req.user._id.toString(), data.influencer_id, "following", function (err, result) {
                                if (err) return callback(err);
                                callback();
                            })
                        })
                    }
                },
                function (callback) {

                    // Remove old board_answers for board_id and replace it with new one
                    user.board_answers = _.filter(user.board_answers, function (board) {
                        return board.board_id.toString() != boardAnswers.board_id.toString()
                    });

                    user.board_answers.push(boardAnswers);

                    var temp = user.profile_answers;
                    for(var i=0;i<boardAnswers.answers.length;i++){
                        //console.log(boardAnswers.answers[i]);
                        temp = _.filter(temp, function(profile_answer){
                            return profile_answer.id.toString() != boardAnswers.answers[i].id.toString()
                        });
                        temp.push(boardAnswers.answers[i]);
                    }

                    user.profile_answers = temp;
                    user.save(function (err) {
                        if (err)
                            return callback(err);

                        callback();
                    })
                }
            ], function (err, result) {
                if (err)
                    return next(err);

                // Send back all of the board_answers so UI can update team page
                res.send(JSON.stringify(user.board_answers));
            })
        })
    }
    catch (e) {
        next(new customError.InvalidContent("Data is not in json format."));
    }

}

function askGeneralQuestion(req, res, next) {

    userModel.getUserByCondition({_id: req.user._id}, function (err, user) {
        if (err)
            return next(err);
        // MB: Let's allow this now for testing
        //   if(user.profile_answers.length>0)
        //       return next(new customError.InvalidArgument("You have already given answers to these questions"));

        questionModel.getQuestionsByCondition({type: "general_question"}, 999, function (err, questions) {
            if (err)
                return next(err);
            console.log("questions: " + JSON.stringify(questions, null, 4));
            var controls = [];
            for (var i = 0; i < questions.length; i++) {
                controls.push(getControl(questions[i]))
            }
            res.send(controls);
        })
    })
}

function saveGeneralQuestion(req, res, next) {
    try {
        var data = req.body instanceof Object ? req.body : JSON.parse(req.body);
        userModel.getUserByCondition({_id: req.user._id}, function (err, user) {
            if (err)
                return next(err);
            var objectToSave = {};
            for (var i = 0; i < data.controls.length; i++) {
                var answer = data.controls[i];
                if (answer.type == "checkbox") {
                    var val = [];
                    var returnVal = [];
                    for (var j = 0; j < answer.options.length; j++) {
                        var option = answer.options[j];
                        if (option.checked) {
                            val.push(option.value);
                            returnVal.push(option.return_value);
                        }
                    }
                    objectToSave = {
                        id: ObjectId(answer.id),
                        value: val,
                        return_value: returnVal,
                        code: answer.code
                    }
                }
                else {
                    objectToSave = {
                        id: ObjectId(answer.id),
                        value: answer.answer.value,
                        return_value: answer.answer.return_value,
                        code: answer.code
                    }
                }
                // remove the previous answer if we're answering it again
                for (var j = 0; j < user.profile_answers.length; j++) {
                    if (user.profile_answers[j].code == objectToSave.code) {
                        user.profile_answers.splice(j, 1);
                        break;
                    }
                }
                user.profile_answers.push(objectToSave);
            }
            var wasRegistered = user.is_registered;
            user.is_registered = true;
            user.save(function (err) {
                // If the user is just registering, load their first offers by calling wine server
                if (!wasRegistered) {
                    var url = config.stores.file.store.host.hostName + ":3001/offers/create_initial";
                    // Wait to hear back from server that sends initial messages
                    request.post(url, {json: {id: user._id.toString()}}, function (error, response, body) {
                        res.send({status: "Profile created."});
                    });
                }
                else {
                    res.send({status: "Profile updated."});
                }
            })
        })
    }
    catch (e) {
        next(new customError.InvalidContent("Data is not in json format."));
    }
}

function getAuthenticatedUser(req, res, next) {
    userModel.getUserByCondition({_id: req.user._id}, function (err, user) {
        res.send(user);
    })
}

function getOffers(req, res, next) {

    // If there is no offer_id, then we'll get the most recent one
    var predicate = req.params.offer_id ? {user_id:req.params.user_id, _id:req.params.offer_id} : {user_id:req.params.user_id};

    offerModel.getOneOffer(predicate, function (err, offers) {
        if (err)
            return next(err);

        res.send(offers);
    });
}

function getOffer(req, res, next) {

    // If there is no offer_id, then we'll get the most recent one
    var predicate = {user_id:req.params.user_id};

    offerModel.getOneOffer(predicate, function (err, offers) {
        if (err)
            return next(err);

        res.send(offers);
    })
}

// Send the next offer to the user
function getSingleOffer(req, res, next) {
    var url = config.stores.file.store.host.hostName + ":3001/offers/next";
    request.post(url, {json: {id: req.user._id.toString()}}, function (error, response, body) {
        res.send("offer request made");
    });
}


function getOfferDetails(req, res, next) {
    if (!utilityFunction.isValidObjectId(req.params.id))
        return next(new customError.InvalidArgument("Offer id is not in correct format."));

    offerModel.getOffers({_id: req.params.id, user_id: req.user._id}, function (err, offers) {
        if (err)
            return next(err);

        res.send(offers.length > 0 ? offers[0] : {});
    })
}

function saveClick(req, res, next) {
    var offerId = req.body.offer_id ? req.body.offer_id : req.params.offer_id;
    var sourceId = req.body.source_id ? req.body.source_id : req.params.source_id;
    if (!utilityFunction.isValidObjectId(offerId) || !utilityFunction.isValidObjectId(sourceId))
        return next(new customError.InvalidArgument("Offer or Source id is not in correct format."));

    offerModel.saveClick(offerId, sourceId, function (err, result) {
        if (err)
            return next(err);

        if (req.params.offer_id)
            res.redirect(result);
        else
            res.send(result);
    })
}

function getUserPreferences(req, res, next) {
    if (!utilityFunction.isValidObjectId(req.params.id))
        return next(new customError.InvalidArgument("User id is not in correct format."));
    // Load master list of preferences and then get any existing prefs for user
    supportModel.findOne({code:"PREFERENCE_DEFAULTS"}, function(err, defaults) {
        if (err)
            return next(new customError.InvalidArgument("Could not get preference defaults from support model."));

        // Get existing user defaults, if any
        userModel.getUserByCondition({_id:req.params.id}, function(err, user) {
            if (err)
                return next(new customError.InvalidArgument("Could not get user."));

            // Go through each section of defaults, add in user saved data and return
            defaults = defaults.toObject();
            var userPrefs = user.preferences.toObject();
            var prefObj = _.object(_.map(userPrefs, function(x){return [x.code, x.value]})); // Convert to object {"code":"value"}

            // Copy over saved values
            defaults.content.basic.forEach(function(item) {
                item.value = prefObj[item.code] || item.value;
            });
            defaults.content.style_prefs.forEach(function(item) {
                item.value = prefObj[item.code] || item.value;
            });
            defaults.content.offer_prefs.forEach(function(item) {
                item.value = prefObj[item.code] || item.value;
            });
            // Send back full structure
            res.send(defaults.content);
        });
    });
}

function setUserPreferences(req, res, next) {
    if (!utilityFunction.isValidObjectId(req.params.id))
        return next(new customError.InvalidArgument("User id is not in correct format."));
    if (!req.body.preferences)
        return next(new customError.InvalidArgument("Preference body is not in correct format."));
    userModel.updateUserFields(req.params.id, {preferences:req.body.preferences}, function(err, result) {
        res.send(result);
    });
}


function emailLatestOffer(req, res, next){
    if (!utilityFunction.isValidObjectId(req.params.user_id))
        return next(new customError.InvalidArgument("User id is not in correct format."));

    var predicate = req.params.offer_id ? {user_id:req.params.user_id, _id:req.params.offer_id} : {user_id:req.params.user_id};

    userModel.getUserByCondition({_id: req.params.user_id}, function (err, existingUser) {
        if (err || existingUser == null) {
            return next(new customError.InvalidArgument("User id is not in correct format."));
        }
        else {
            offerModel.getOneOffer(predicate, function (err, offer) {
                if (err)
                    return next(err);

                emailTemplates(templatesDir, function(err, template) {

                  // Render a single email with one template

                  var dateObj = new Date();
                  var monthArray = ["January","February","March","April","May","June","July","August","September","October","November","December"];
                  var locals = { offer: offer,user:existingUser, dateString:monthArray[dateObj.getMonth()] + " " + dateObj.getDate() + ", " + dateObj.getFullYear() };

                  template('offer', locals, function(err, html, text) {


                    // var transport = nodemailer.createTransport("SMTP", {
                    //   service: "Gmail",
                    //   auth: {
                    //     user: "************",
                    //     pass: "************"
                    //   }
                    // });


                    // template('offer', locals, function(err, html, text) {
                    //   if (err) {
                    //     return next(err);
                    //   } else {
                    //     transport.sendMail({
                    //       from: 'Cruzu <noreply@cruzu.com>',
                    //       to: existingUser.email,
                    //       subject: 'Cruzu Offer',
                    //       html: html,
                    //       // generateTextFromHTML: true,
                    //       text: text
                    //     }, function(err, responseStatus) {
                    //       if (err) {
                    //         return next(err);
                    //       } else {
                    //         res.send(responseStatus.message);
                    //       }
                    //     });
                    //   }
                    // });

                    
                    var email = new sendgrid.Email({
                        to: existingUser.email,
                        toname: existingUser.name,
                        from: "noreply@cruzu.com",
                        fromname: "Cruzu Offer",
                        subject: "Cruzu Offer ",
                        html: html
                    });
                    sendgrid.send(email, function (err, json) {
                        if (err)
                            res.send(err);
                        res.send(json);
                    });
                  });
                });
            });
        }
    });
}


// Email feedback from a user about an offer
function sendFeedback(req, res, next) {

    var data = req.body instanceof Object ? req.body : JSON.parse(req.body);
    var user_name = req.user.name;
    var user_id = req.user._id;

    var email = new sendgrid.Email({
        to: "michael@cruzu.com",
        toname: "Michael Brill",
        from: "noreply@cruzu.com",
        fromname: "Cruzu Feedback",
        subject: "Feedback from " + user_name + " (" + user_id + ") about offer id:" + data.id,
        html: data.feedback
    });
    sendgrid.send(email, function (err, json) {
        if (err)
            console.error(err);
        res.send(0);
    });
}

function updateProfile(req, res, next) {
    var data = req.body instanceof Object ? req.body : JSON.parse(req.body);

    userModel.getUserByCondition({_id: req.user._id}, function (err, user) {
        if (err)
            return next(err);

        var userObject = user.toObject();
        var dataToUpdate = {};
        for (var key in data) {
            if (_.has(userObject, key)) {
                dataToUpdate[key] = data[key];
            }
            else if (key == "city" || key == "state_region" || key == "postal_code" || key == "country") {
                data["address." + key] = data[key];
            }
        }

        userModel.updateUserFields(user._id.toString(), dataToUpdate, function (err, result) {
            console.log(err);
            res.send(result);
        })
    })
}

function getControl(question) {
    var control = {};
    switch (question.answer.control_type) {
        case "range":
            control["id"] = question._id;
            control["title"] = question.text;
            control["type"] = "range";
            control["range"] = question.answer.range;
            control["answer"] = "";
            control["code"] = question.code;
            break;
        case "text":
            control["id"] = question._id;
            control["title"] = question.text;
            control["type"] = "text";
            control["answer"] = "";
            control["code"] = question.code;
            break;
        case "textarea":
            control["id"] = question._id;
            control["title"] = question.text;
            control["type"] = "textarea";
            control["answer"] = "";
            control["code"] = question.code;
            break;
        case "radiobutton":
            control["id"] = question._id;
            control["title"] = question.text;
            control["type"] = "radio";
            var options = [];
            for (var i = 0; i < question.answer.option_list.length; i++) {
                options.push({value: question.answer.option_list[i].display_value, return_value: question.answer.option_list[i].return_value});
            }
            control["options"] = options;
            control["answer"] = "";
            control["code"] = question.code;
            break;
        case "dropdown":
            control["id"] = question._id;
            control["title"] = question.text;
            control["type"] = "dropdown";
            var options = [];
            for (var i = 0; i < question.answer.option_list.length; i++) {
                options.push({value: question.answer.option_list[i].display_value, return_value: question.answer.option_list[i].return_value});
            }
            control["options"] = options;
            control["answer"] = "";
            control["code"] = question.code;
            break;
        case "checkbox":
            control["id"] = question._id;
            control["title"] = question.text;
            control["type"] = "checkbox";
            control["default_text"] = question.answer.default_text;
            var options = [];
            for (var i = 0; i < question.answer.option_list.length; i++) {
                options.push({value: question.answer.option_list[i].display_value, return_value: question.answer.option_list[i].return_value, checked: false});
            }
            control["options"] = options;
            control["answer"] = "";
            control["code"] = question.code;
            break;
    }
    return control;
}

function addToMailingList(req, res, next) {

    if (!req.query.email)
        return next(new customError.InvalidArgument("email query param missing"));


    // Temporarily send a copy of the email since MailChimp API is unreliable
    var sendgrid = require('sendgrid')("cruzu", "wineisawesome");
    var Email = sendgrid.Email;
    var email = new Email({
        to: "michael@cruzu.com",
        toname: "Michael Brill",
        from: "michael@cruzu.com",
        fromname: "USER SUBSCRIBE",
        subject: "USER SUBSCRIBE",
        html: req.query.email
    });
    sendgrid.send(email, function (err, json) {
        if (err) {
            console.error(err);
        }
    });

    var api = new MailChimpAPI('9d5bd7d85e3e17dd5c777ad1808f2960-us6', { version : '2.0' });
    var listId = "76736af2e5";
    api.call('lists', 'subscribe', { id: listId, email: {email: req.query.email}}, function (error, data) {
        if (error) {
            console.log(error.message);
        }
        res.send(error);
    });
}

