var config = require('../config/config'),
    questionModel = require('../models/question'),
    utilityFunction = require('../lib/utility-functions'),
    customError = require('../lib/custom_errors'),
    async = require('async');

exports.setup = function (app) {
    app.post('/question/add', utilityFunction.ensureAuthenticated, addQuestion);
    app.get('/question/all', utilityFunction.ensureAuthenticated, getAllQuestions);
}

function addQuestion(req, res, next) {
    try {
        var data = req.body instanceof Object ? req.body : JSON.parse(req.body);
        questionModel.addQuestion(data, function (err, result) {
            if (err)
                return next(err);

            res.send(result);
        })
    }
    catch (e) {
        next(new customError.InvalidContent("Data is not in json format."));
    }
}


function getAllQuestions(req, res, next) {

    questionModel.getQuestionsByCondition({type:"board_question"}, 999, function (err, result) {
        if (err)
            return next(err);

        res.send(result);
    })

}
