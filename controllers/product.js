var productModel = require('../models/product'),
    customError = require('../lib/custom_errors'),
    boardModel = require('../models/board'),
    async = require('async'),
    request = require('request'),
    passport = require('passport'),
    config = require('../config/config'),
    ObjectId = require('mongoose').Types.ObjectId,
    utilityFunction = require('../lib/utility-functions');

exports.setup = function (app) {
    app.get('/product/:searchText/list', utilityFunction.ensureAuthenticated, searchProducts);
    app.get('/product/:searchText/list_active', utilityFunction.ensureAuthenticated, searchActiveProducts);
    app.get('/product/:id', utilityFunction.ensureAuthenticated, getSingleActiveProduct);
    app.get('/product/active/:product_id', utilityFunction.ensureAuthenticated, getSingleActiveProductByProductID);
    app.get('/product/wines', utilityFunction.ensureAuthenticated, getWines);
    app.post('/product/add', utilityFunction.ensureAuthenticated, addProductToBoard);
    app.post('/product/mobile/:source_id/feedback', passport.authenticate('bearer', { session: false }), postProductFeedback);
    app.get('/product/mobile/:searchText/list_active', passport.authenticate('bearer', { session: false }), searchActiveProducts);
    app.post('/product/mobile/delink_product_source', passport.authenticate('bearer', { session: false }), removeSourceLink);
};

function postProductFeedback(req, res, next) {

    if (!req.body.user_id || !req.params.source_id)
        return next(new customError.MissingParameter("Missing required parameter fo postProductFeedback"));

    // Send an email with broken product info
    utilityFunction.sendEmail("info@cruzu.com", "info@cruzu.com", "info@cruzu.com","Cruzu App", "Broken Product "
            +req.params.source_id, "User "+req.body.user_name + "("+req.body.user_id +") has reported the product with source/list_source _id of "
            +req.params.source_id+" is broken.  \nProduct Name: "+req.body.product_name+"\n\nComment: "+req.body.comment,
    function() {
        res.send(null);
    });
}

function removeSourceLink(req, res, next) {
    if (!req.body.product_id || !req.body.source_id)
        return next(new customError.MissingParameter("Missing product_id or source_id"));

    request.post(config.get('queryURLBase') + "/delink_product_source",
        {
            json: req.body
        }, function (err, response, body) {
            if (err)
                return next(new customError.HttpRequestRejected("Failed to get data"));

            res.send(body);
        })
}


function searchProducts(req, res, next){
    if(!req.params.searchText)
        return next(new customError.MissingParameter("Missing required parameter"));

    productModel.searchProducts(req.params.searchText,function(err, results){
        if(err)
            return next(err);

        boardModel.getBoard({owner_id: req.user._id}, function(err, board){
            if(err)
                return next(err);

            var products = [];
            for(var i=0;i<results.results.length;i++){
                var data = results.results[i];
                var product = {};
                if(data.obj && data.obj.vintages.length>0){
                    product["id"] = data.obj._id;
                    product["name"] = data.obj.meta.name;
                    product["price"] = "";
                    product["vintages"] = getProductVintages(data.obj.vintages);
                    product["isAdded"] = true;
                    if(product.vintages.length > 0){
                        var temp = {
                            _id: data.obj._id,
                            vintages: product.vintages
                        };
                        product.vintages = isProductExistInBoard(temp, board.products);
                        products.push(product);

                    }
                    else
                        product["isAdded"] = true;
                }
            }

            res.send(products);
        })
    })
}

function searchActiveProducts(req, res, next){
    if(!req.params.searchText)
        return next(new customError.MissingParameter("Missing required parameter"));

    // Look for a vintage. If no vintage then assume a 'NV' wine. Then remove it from search string
    var searchText = req.params.searchText;
    var years = searchText.match(/\b\d{4}\b/g);
    searchText = searchText.replace(/\bnv\b/i,"");          // Remove NV as absence of date is same thing
    var vintage = null;         // Assume NV
    if (years) {
        vintage = parseInt(years[0]);
        searchText = searchText.replace(years[0],"").trim();
    }

    productModel.searchActiveProducts(searchText, vintage, function(err, results){
        if(err)
            return next(err);

        // Now reattach vintage if there is one.
        results.forEach(function(result) {
            result.meta.name = (result.vintage ? result.vintage : "NV")+" "+result.meta.name;
        });

        res.send(results);
    });
}


function getSingleActiveProduct(req, res, next){
    if(!req.params.id)
        return next(new customError.MissingParameter("Missing required parameter"));

    productModel.getSingleActiveProduct(ObjectId(req.params.id), function(err, result){
        if(err)
            return next(err);

        res.send(result);
    });
}

function getSingleActiveProductByProductID(req, res, next) {
  if (!req.params.product_id)
      return next(new customError.MissingParameter("Missing required parameter"));

    productModel.getSingleActiveProductFromProductID(ObjectId(req.params.product_id), req.query.vintage, function(err, result) {
        if (err)
          return next(err);

        res.send(result);
    });
}




function addProductToBoard(req, res, next){
    if(!req.body.product)
        return next(new customError.MissingParameter("Missing required parameter"));

    var product = req.body.product;

    var productToSave = {};
    productToSave["product_id"] = product["id"];
    productToSave["product_name"] = product["name"];
    productToSave["vintage"] = product.vintage.year;
    productToSave["non_vintage"] = product.vintage.non_vintage;

    boardModel.addProduct(req.user._id,productToSave,function(err, board){
        if(err)
            return next(err);

        res.send(board);
    })
}

function getWines(req, res, next){
    boardModel.getBoard({owner_id: req.user._id}, function(err, board){
        if(err)
            return next(err);

        async.forEach(board.products,function(product, cb){
            productModel.getProduct({_id: product.product_id}, function(err, result){
                if(err || result==null)
                cb();
                else{
                    var vintages = getProductVintages(result.vintages);
                    for(var i=0; i<vintages.length;i++){
                        if(vintages[i].year==product.vintage){
                            product["price"] = "$" + vintages[i].lowest_price;
                            vintages[i].highest_price ? product.price += " - $" + vintages[i].highest_price : "";
                            break;
                        }
                    }
                    product.note ? "" : product["note"] = "";
                    product.review ? "" : product["review"] = "";
                    cb();
                }
            })
        },function(err){
            res.send(board.products);
        })
    })
}

function isProductExistInBoard(product, products){
    var exist = false;
    for(var i=0;i<products.length;i++){
        if(products[i].product_id == product._id.toString()){
            for(var k=0;k < product.vintages.length; k++){
                var vintage = product.vintages[k];
                if(products[i].vintage==vintage.year){
                    vintage["isAdded"] = true;
                }
                else
                    vintage["isAdded"] = false;
            }
        }
    }

    return product.vintages;
}

function getProductVintages(productVintages){
    var vintages = [];
    for(var k=0;k<productVintages.length;k++){
        var vintageData = productVintages[k];
        var vintage = {};
        if(vintageData.vintage){
            vintage["year"] = vintageData.vintage;
            vintage["sortField"] = vintageData.vintage;
            vintage["non_vintage"] = false;
        }
        else{
            vintage["year"] = "NV";
            vintage["sortField"] = 0;
            vintage["non_vintage"] = true;
        }

        var sources = [];
        for(var j=0;j<vintageData.sources.length;j++){
            var source = vintageData.sources[j];
            if(source.qoh>0){
                sources.push({
                    name: source.source_name,
                    price: source.actual_price
                })
            }
        }
        vintage["sources"] = sources;
        if(sources.length>0){
            sources.sort(function(a,b) { return parseFloat(a.price) - parseFloat(b.price) } );
            vintage["lowest_price"] = sources[0].price;
            sources.length > 1 ? vintage["highest_price"] = sources[sources.length-1].price : "";
            vintages.push(vintage);
        }

    }

    vintages.sort(function(a,b) { return parseFloat(b.sortField) - parseFloat(a.sortField) } );
    return vintages;
}