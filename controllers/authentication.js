var passport = require('passport'),
    config = require('../config/config'),
    userModel = require('../models/user'),
    varietalModel = require('../models/varietal'),
    regionModel = require('../models/region'),
    customError = require('../lib/custom_errors'),
    _ = require('underscore')._,
    utilityFunction = require('../lib/utility-functions');

exports.setup = function (app) {
    app.get('/logout', logout);
    app.get('/auth/service/:service_name', oauth);
    app.get('/auth/service/:service_name/callback', oauthCallback);
    app.post('/mobile/user/login', loginMobileUser);
}


function oauth(req, res, next) {
    var options = {};
    if (req.params.service_name.toLowerCase() == "facebook")
        options = { display: 'popup', scope: ['email', 'user_hometown'] };
    passport.authenticate(req.params.service_name, options)(req, res, next);
}

function oauthCallback(req, res, next) {
    passport.authenticate(req.params.service_name, function (err, user, info) {
        if (err) {
            return next(err);
        }
        if (!user) {
            return res.redirect('/');
        }

        userModel.getUserByCondition({fb_id: user.fb_id}, function (err, existingUser) {
            if (err || existingUser == null) {
                userModel.registerUser(user, function (err, user) {
                    req.logIn(user, function (err) {
                        if (err) {
                            return next(err);
                        }
                        res.send('<html><head><script type="text/javascript">window.close();</script></head></html>');
                    })
                })
            }
            else {
                userModel.updateUserFields(existingUser._id.toString(), {fb_access_token: user.fb_access_token}, function (err, result) {
                    req.logIn(existingUser, function (err) {
                        if (err) {
                            return next(err);
                        }
                        res.send('<html><head><script type="text/javascript">window.close();</script></head></html>');
                    })
                })
            }
        })
    })(req, res, next);
}

function loginMobileUser(req, res, next) {
    var verification = utilityFunction.verifyParameters(req.body, ["fb_id", "fb_access_token"]);
    if (verification.isError)
        return next(new customError.MissingParameter("Required parameters are missing."));

    userModel.getUserByCondition({fb_id: req.body.fb_id}, function (err, existingUser) {
        if (err)
            return next(new customError.Database(err.toString()));

        // Return varietals and regions along with login data (probably belongs in its own API call)
        varietalModel.Varietals.find({}).select('name color').sort("field name").lean().exec(function (err, varietals) {
                varietals = _.map(varietals, function(varietal) {delete varietal._id; return varietal});
               regionModel.Regions.find({}).select('region regions country').sort('field region').lean().exec(function (err, regions) {
                   regions = _.map(regions, function(region) {delete region._id; return region});
                if (existingUser == null) {
                    userModel.registerUser(req.body, function (err, user) {
                        if (err)
                            return next(new customError.Database(err.toString()));
                        user = user.toObject();
                        user.all_varietals = varietals;
                        user.all_regions = regions;
                        res.send(user);
                    })
                }
                else {
                    userModel.updateUserFields(existingUser._id.toString(), {fb_access_token: req.body.fb_access_token}, function (err, result) {
                        if (err)
                            return next(new customError.Database(err.toString()));
                        existingUser = existingUser.toObject();
                        existingUser.all_varietals = varietals;
                        existingUser.all_regions = regions;
                        res.send(existingUser);
                    })
                }
            })
        })
    })
}

function logout(req, res, next) {
    req.logout();
    res.redirect('#/intro');
}


