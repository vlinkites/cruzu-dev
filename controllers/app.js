var customError = require('../lib/custom_errors'),
    venueModel = require('../models/venue'),
    appModel = require('../models/app'),
    boardModel = require('../models/board'),
    userModel = require('../models/user'),
    async = require('async'),
    ObjectId = require('mongoose').Types.ObjectId,
    config = require('../config/config'),
    request = require('request'),
    passport = require('passport'),
    utilityFunction = require('../lib/utility-functions'),
    _ = require('underscore'),
    util = require('../lib/utility-functions');


exports.setup = function(app) {
    app.get('/app', utilityFunction.ensureAuthenticated, getAppsForUser);
    app.post('/app/new', utilityFunction.ensureAuthenticated, createApp);
    app.post('/app/:app_id', utilityFunction.ensureAuthenticated, saveApp);
    app.delete('/app/:app_id', utilityFunction.ensureAuthenticated, deleteApp);

};


// Get all the apps owned by the user
function getAppsForUser(req, res, next) {

    appModel.getAppsForUser( ObjectId(req.user._id), function(err, result) {
        if(err)
            return next(err);

        res.send(result);
    });
}

// Create new app
function createApp(req, res, next) {

    appModel.createApp(req.body.app, function(err, result) {
        if(err)
            return next(err);

        res.send(result);
    });
}



// Upsert the app
function saveApp(req, res, next){

    appModel.upsertApp( ObjectId(req.params.app_id), req.body.app, function(err, result) {
        if(err)
            return next(err);

        res.send(result);
    });
}


// Delete the app
function deleteApp(req, res, next){

    appModel.markAppAsDeleted( ObjectId(req.params.app_id), function(err, result) {
        if(err)
            return next(err);

        res.send(result);
    });
}



