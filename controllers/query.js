var customError = require('../lib/custom_errors'),
    queryModel = require('../models/query'),
    async = require('async'),
    ObjectId = require('mongoose').Types.ObjectId,
    config = require('../config/config'),
    request = require('request'),
    passport = require('passport'),
    utilityFunction = require('../lib/utility-functions'),
    _ = require('underscore'),
    util = require('../lib/utility-functions');


exports.setup = function(app) {
    app.get('/query/attributes/all', utilityFunction.ensureAuthenticated, getAllQueryAttributesAndListValues);
    app.post('/query/adhoc', utilityFunction.ensureAuthenticated, processAdHocQuery);
};


// Get all the apps owned by the user
function getAllQueryAttributesAndListValues(req, res, next) {

    queryModel.getAllQueryAttributesAndListValues(function(err, result) {
        if(err)
            return next(err);

        res.send(result);
    });
}

// Call out to wine query server to process request
function processAdHocQuery(req, res, next) {

    var url = config.stores.file.store.host.hostName + ":3001/query/adhoc";
    request.post(url, {json: req.body}, function (error, response, body) {
        res.send(body);
    });
}

