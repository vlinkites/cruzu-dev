var customError = require('../lib/custom_errors'),
    boardModel = require('../models/board'),
    ObjectId = require('mongoose').Types.ObjectId,
    utilityFunction = require('../lib/utility-functions'),
    _ = require('underscore');

exports.setup = function (app) {
    app.put('/board/updateProducts', utilityFunction.ensureAuthenticated, updateProductInBoard);
    app.put('/board/deleteProducts', utilityFunction.ensureAuthenticated, deleteProductInBoard);
    app.get('/board/agent/:agent_id', utilityFunction.ensureAuthenticated, getBoardListForAgent);
    app.post('/board/new', utilityFunction.ensureAuthenticated, createBoard);
    app.post('/board/:board_id', utilityFunction.ensureAuthenticated, updateBoard);
    app.delete('/board/:board_id', utilityFunction.ensureAuthenticated, deleteBoard);
};


function createBoard(req, res, next) {

    req.body._id = new ObjectId();
    updateBoardStructureForNewStyle(req.body, function(board) {
        new boardModel(req.body).save(function(err, board){
            if(err)
                return callback(new customError.Database(err.toString()),null);

            res.send(board);
        });
    })
}

function updateBoard(req, res, next) {

    var id = req.body._id;
    delete req.body._id;
    updateBoardStructureForNewStyle(req.body, function(board) {
        boardModel.findOneAndUpdate({_id:id}, req.body, {}, function(err, board) {
            if(err)
                return callback(new customError.Database(err.toString()),null);

            res.send(board);
        });
    })
}



function deleteBoard(req, res, next) {

    boardModel.remove({ _id: req.params.board_id }, function(err) {

        if(err)
            return callback(new customError.Database(err.toString()),null);

        res.send(err);

    });
}



function getBoardListForAgent(req, res, next) {

    if (!utilityFunction.isValidObjectId(req.params.agent_id))
        return next(new customError.InvalidArgument("agent_id is not in correct format."));

    boardModel.getBoardsForAgent(ObjectId(req.params.agent_id), function(err, boards){

        if(err)
            return next(err);

        res.send(boards);
    });
}


function updateProductInBoard(req, res, next){
    console.log(req.body.products);
    updateBoard("update", req.body.products, req.user._id, function(err, board){
        if(err)
            return next(err);

        res.send(board);
    })
}

function deleteProductInBoard(req, res, next){
    updateBoard("delete", req.body.products, req.user._id, function(err, board){
        if(err)
            return next(err);

        res.send(board);
    })
}


function updateBoardStructureForNewStyle(board, cb) {

    // Only update if this is a new type of board and it has sections
    if (!board.is_new_type)
        return cb(board);
    if (!board.sections || board.sections.length==0)
        return cb(board);
    var attributes = board.sections[0].predicate;

    attributes.forEach(function(attribute) {

        var userOptions = [];
        var defaultValues = [];
        var childValue;
        var inOptionMode = false;
        var inDefaultMode = true;
        var inChildrenMode = false;

        // Process all of the tag inputs
        if (["TYPE","STYLE","VARIETAL","REGION"].indexOf(attribute.attribute) > -1) {
            // Define the type of UI object we'll present user with
            attribute.ui_type = ["VARIETAL","REGION"].indexOf(attribute.attribute) > -1 ? "tags" : "buttons";
            attribute.option_any = false;
            attribute.value.forEach(function(token) {
                if (token=="#DEFAULT") {
                    inDefaultMode = true;
                    inOptionMode = false;
                    inChildrenMode = false;
                }
                else if (token=="#OPTIONS") {
                    inDefaultMode = false;
                    inOptionMode = true;
                    inChildrenMode = false;
                }
                else if (token=="#ANY" && inOptionMode) {
                    attribute.option_any = true;
                }
                else if (token=="#CHILDRENOF" && inOptionMode) {
                    inChildrenMode = true;
                }
                // We now have a value, not a keyword
                else if (inChildrenMode) {
                    childValue = token;
                }
                else if (inOptionMode) {
                    userOptions.push(token);
                }
                else if (inDefaultMode) {
                    defaultValues.push(token);
                }
            });
            attribute.options = _.compact(userOptions) || [];
            attribute.defaults = _.compact(defaultValues) || [];
            attribute.children_of = childValue;

        }
        // Otherwise it's a value range attribute & how we have it stored is fine
        else {
            attribute.ui_type = "slider";
            attribute.defaults = attribute.value;
        }
    });
    return cb(board);
}


/*
function updateBoard(action, products, ownerId, callback){
    boardModel.getBoard({owner_id: ownerId}, function(err, board){
        if(err)
            return callback(err, null);

        for(var i=0; i<products.length;i++){
            var product = products[i];
            for(var j=0;j<board.products.length;j++){
                if(product.product_id==board.products[j].product_id){
                    if(action=='update'){
                        var temp = board.products[j];
                        temp.note = product.note;
                        temp.review = product.review;
                        board.products.splice(j,1);
                        board.products.splice(j, 0, temp);
                        break;
                    }
                    else if(action == 'delete'){
                        board.products.splice(j,1);
                        break;
                    }
                }
            }
        }

        board.save(function(err){
            console.log(err);
            if(err)
                return callback(err, null);

            callback(null, board);
        })
    })
}
*/
