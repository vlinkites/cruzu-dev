var config = require('../config/config'),
    offerModel = require('../models/offer_history'),
    utilityFunction = require('../lib/utility-functions'),
    customError = require('../lib/custom_errors'),
    async = require('async');

exports.setup = function(app) {
    app.put('/offer/:offer_id/source/:source_id/save_click',utilityFunction.ensureAuthenticated,saveClick);
}


function saveClick(req, res, next){
    if(!utilityFunction.isValidObjectId(req.params.offer_id) || !utilityFunction.isValidObjectId(req.params.source_id))
        return next(new customError.InvalidArgument("Offer or Source id is not in correct format."));

    offerModel.saveClick(req.params.offer_id,req.params.source_id,function(err,result){
        if(err)
            return next(err);

        res.send(result);
    })
}
