$(function () {
    
    /*************************************************/
    /************ #rootwizard-custom-arrow ***********/
    $('#rootwizard-tabdetail2').bootstrapWizard({
        onTabShow: function(tab, navigation, index) {
            var $total = navigation.find('li').length;
            var $current = index+1;
            var $percent = ($current/$total) * 100;
            $('#rootwizard-tabdetail2').find('.bar').css({width:$percent+'%'});
        },
        'onNext': function(tab, navigation, index) {

            // select id of current tab content
            var $id = tab.find("a").attr("href");
            var $approved = 1;

            $($id + " input").each(function(){
                if (!$(this).val()) {
                    $(this).parent().parent().find("i.alert").removeClass("alert-hide");
                    $approved = 0;
                } else {
                    $(this).parent().parent().find("i.alert").addClass("alert-hide");
                }
            });
            if ($approved !== 1) return false;
        },
        'onTabClick': function(tab, navigation, index) {
            // select id of current tab content
            var $id = tab.find("a").attr("href");
            var $approved = 1;
            // Check all input validation
            $($id + " input").each(function(){
                if (!$(this).val()) {
                    $(this).parent().parent().find("i.alert").removeClass("alert-hide");
                    $approved = 0;
                } else {
                    $(this).parent().parent().find("i.alert").addClass("alert-hide");
                }
            });
            if ($approved !== 1) return false;
            // Add class visited to style css
            if (tab.attr("class")=="visited"){
                tab.removeClass("visited");
            } else {
                tab.addClass("visited");
            }
        },'tabClass': 'nav nav-pills','nextSelector': '.button-next', 'previousSelector': '.button-previous'
    });
    /************ #rootwizard-custom-circle ***********/
    $('#rootwizard-custom-circle').bootstrapWizard({
        onTabShow: function(tab, navigation, index) {
            var $total = navigation.find('li').length;
            var $current = index+1;
            var $percent = ($current/$total) * 100;
            $('#rootwizard-custom-circle').find('.bar').css({width:$percent+'%'});
        },
        'onNext': function(tab, navigation, index) {

            // select id of current tab content
            var $id = tab.find("a").attr("href");
            var $approved = 1;
            // Check all input validation
            $($id + " input").each(function(){
                if (!$(this).val()) {
                    $(this).css('border-color', 'red');
                    $(this).parent().parent().find("i.alert").removeClass("alert-hide");
                    $approved = 0;
                } else if($(this).attr("type") == "email"){
                    //validating email[type="email"]
                    var reEmail = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
                    if(!reEmail.test($(this).val())){
                        $(this).css('border-color', 'red');
                        $(this).parent().parent().find("i.alert").removeClass("alert-hide");
                        $approved = 0;
                    } else {
                        $(this).parent().parent().find("i.alert").addClass("alert-hide");
                    }
                } else if($(this).attr("type") == "string"){
                    //validating email[type="string"]
                    if($(this).attr("name") == "postal_code"){
                        //validating email[name="postal_code"]
                        var re5digit=/^\d{5}$/;
                        if(!re5digit.test($(this).val())){
                            $(this).css('border-color', 'red');
                            $(this).parent().parent().find("i.alert").removeClass("alert-hide");
                            $approved = 0;
                        } else {
                            $(this).parent().parent().find("i.alert").addClass("alert-hide");
                        }
                    }
                } else {  
                    $(this).parent().parent().find("i.alert").addClass("alert-hide");
                }
            });
            //adding validation in select box
            $($id + " select").each(function(){
                if(!$(this).val() || isNaN($(this).val())){
                    $(this).css('border-color', 'red');
                    $(this).parent().parent().find("i.alert").removeClass("alert-hide");
                    $approved = 0;
            } else {
                $(this).parent().parent().find("i.alert").addClass("alert-hide");
            }
            });
            if ($approved !== 1) return false;
        },
        'onTabClick': function(tab, navigation, index) {
            // select id of current tab content
            var $id = tab.find("a").attr("href");
            var $approved = 1;
            // Check all input validation
            $($id + " input").each(function(){
                if (!$(this).val()) {
                    $(this).css('border-color', 'red');
                    $(this).parent().parent().find("i.alert").removeClass("alert-hide");
                    $approved = 0;
                } else {
                    $(this).parent().parent().find("i.alert").addClass("alert-hide");
                }
            });
            if ($approved !== 1) return false;
            // Add class visited to style css
            if (tab.attr("class")=="visited"){
                tab.removeClass("visited");
            } else {
                tab.addClass("visited");
            }
        },
        'tabClass': 'bwizard-steps-o','nextSelector': '.button-next', 'previousSelector': '.button-previous'
    });

});