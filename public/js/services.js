'use strict';

/* Services */

angular.module('cruzu.services', ['ngResource']).
    factory('User', function ($resource) {
        return $resource('/user/:id/:action', {id: '@id', action: '@action'}, {
            getAuthenticatedUser: {method: 'GET', params: {action: "authenticated"}},
            getInfluencers: {method: 'GET', params: {action: "influencers"}, isArray: true},
            getOffers: {method: 'GET', params: {action: "offers"}, isArray: true},
            getOfferDetails: {method: 'GET', params: {action: "offer"}},
            followUser: {method: 'PUT', params: {action: "follow"}},
            getBoard: {method: 'GET', params: {action: "board"}, isArray: false},
            followUserWithBoard: {method: 'POST', params: {action: "follow_user_board"}, isArray: true},
            askGeneralQuestion: {method: 'GET', params: {action: "general_questions"}, isArray: true},
            saveGeneralQuestion: {method: 'POST', params: {action: "general_questions"}},
            saveClick: {method: 'POST', params: {action: "save_click"}},
            sendFeedback: {method: 'POST', params: {action: "send_feedback"}},
            updateProfile: {method: 'POST', params: {action: "update_profile"}},
            getNextOffer: {method: 'GET', params:{action: "next_offer"}}
        });
    }).
    factory('Product', function ($resource) {
        return $resource('/product/:id/:action', {id: '@id', action: '@action'}, {
            getProducts: {method: 'GET', params: {action: "list"}, isArray: true},
            getWines: {method: 'GET', params: {action: "wines"}, isArray: true},
            addProduct: {method: 'POST', params: {action: "add"}},
            getProduct: {method: 'GET'}
        });
    }).
    factory('ActiveProduct', function($resource) {
        return $resource('/product/active/:product_id?vintage=:vintage', {product_id: '@product_id', vintage: '@vintage'}, {
            getProduct: {method: 'GET'}
        });
    }).
    factory('Board', function ($resource) {
        return $resource('/board/:id/:action', {id: '@id', action: '@action'}, {
            updateProducts: {method: 'PUT', params: {action: "updateProducts"}},
            deleteProducts: {method: 'PUT', params: {action: "deleteProducts"}},
            deleteBoard: {method: 'DELETE'}
        });
    }).
    factory('Agent', function($resource) {
        return $resource('/board/agent/:id', {id: '@id'}, {
            getBoards: {method: 'GET', isArray: true}
        });
    }).
    factory('Advisor', function($resource) {
        return $resource('/web/agent/:agent_id/items', {agent_id: '@agent_id', count: '@count'}, {
            getItems: {method: 'POST', params: {count: "5"}, isArray: true}
        });
    }).
    factory('Venue', function($resource) {
        return $resource('/web/venue/all', {}, {
            getVenues: {method: 'GET', isArray: true}
        });
    }).
    factory('App', function($resource) {
        return $resource('/app/:app_id/:action', {app_id: '@app_id', action: '@action'}, {
            getApps: {method: 'GET', isArray: true},
            update: {method: 'POST'},
            newApp: {method: 'POST', params: {action: 'new'}},
            deleteApp: {method: 'DELETE'}
        });
    }).
    factory('Question', function($resource) {
        return $resource('/question/all', {}, {
            getAll: {method: 'GET', isArray: true}
        });
    }).
    factory('Attribute', function($resource) {
        return $resource('/query/attributes/all', {}, {
            getAll: {method: 'GET', isArray: true}
        });
    }).
    factory('Follow', function($modal, User) {
        return {
            update_count: function(bool) {
                bool ? this.count.current++ : this.count.current--;
            },

            count: {
                current: 0,
                init:
                    function(influencers) {
                        var count = 0;
                        for (var i = 0; i < influencers.length; i++) {
                            if (influencers[i].is_follower) {
                                count++;
                            }
                        }
                        this.current = count;
                    }
            },

            follow: 
                function(scope, rootScope, index) { 
                    var self = this;

                    User.followUser({id: scope.influencers[index].id},
                        function (value, responseHeaders) {
                            if (value.status == "ask question") {
                                var modalInstance = $modal.open({
                                    templateUrl: 'partials/influencer_questions.html',
                                    controller: influencerQuestionCtrl,
                                    backdrop: 'static',
                                    resolve: {
                                        influencer: function () { return scope.influencers[index]; },
                                        board: 
                                            function ($q, User) {
                                                var deferred = $q.defer();
                                                var callback = function (result) { deferred.resolve(result); };
                                                User.getBoard({id: scope.influencers[index].id}, callback);
                                                return deferred.promise;
                                            }
                                        }
                                    });

                                    modalInstance.result.then(function (message) {
                                        rootScope.message = message;
                                        $modal.open({
                                            templateUrl: 'modal.html',
                                            controller: confirmationModalCtrl
                                        });
                                        self.update_count(value.status.indexOf('not') == -1);
                                    }, function () {
                                    });
                                }
                            else {
                                self.update_count(value.status.indexOf('not') == -1);
                            }
                        }, function (errorResponse) {
                            alert(errorResponse.data.error);
                        });
            }
        };
    });
