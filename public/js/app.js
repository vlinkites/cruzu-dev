'use strict';

// Declare app level module which depends on filters, and services

angular.module('cruzu', [
        'ngRoute',
        'ngAnimate',
        'cruzu.loginController',
        'cruzu.teamController',
        'cruzu.generalQuestionController',
        'cruzu.offerController',
        'cruzu.offerDetailsController',
        'cruzu.profileController',
        'cruzu.boardController',
        'cruzu.exploreController',
        'cruzu.appsController',
        'cruzu.boardsController',
        'cruzu.testFlightController',
        'cruzu.filters',
        'cruzu.services',
        'cruzu.directives',
        'ui.bootstrap',
        'ui.slider',
        'ui.multiselect',
        'wu.masonry',
        'decipher.tags',
        'cruzu.userPreferencesController'
      
]).
config(function ($routeProvider, $locationProvider) {
  $routeProvider.
      when('/login', {
          templateUrl: 'partials/login.html',
          controller: 'loginCtrl'
      }).
      when('/main', {
          templateUrl: 'partials/main.html#top',
          controller: 'mainCtrl',
          resolve: {
              influencers: function($q, User) {
                  var deferred = $q.defer();
                  var callback = function(result) {
                      deferred.resolve(result);
                  };

                  User.getInfluencers(callback);
                  return deferred.promise;
              }
          }
      }).
      when('/influencer_questions', {
          templateUrl: 'partials/influencer_questions.html',
          controller: 'influencerQuestionCtrl'
      }).
      when('/general_questions', {
          templateUrl: 'partials/general_questions.html',
          controller: 'generalQuestionCtrl',
          resolve: {
              controls: function($q, User) {
                  var deferred = $q.defer();
                  var callback = function(result) {
                      deferred.resolve(result);
                  };

                  User.askGeneralQuestion(callback);
                  return deferred.promise;
              }
          }
      }).
      when('/offer_details/:offer_id', {
          templateUrl: 'partials/offer_detail.html',
          controller: 'offerDetailsCtrl',
          resolve: {
            offer: function($q, $route, User) {
              var deferred = $q.defer();
              var callback = function(result) {
                deferred.resolve(result);
              };

              User.getOfferDetails({id: $route.current.params.offer_id}, callback);
              return deferred.promise;
            }
          }
      }).
      when('/intro', {
         templateUrl: 'partials/intro.html',
          controller: 'introCtrl'
      }).
      when('/wine_sellers', {
          templateUrl: 'partials/wine_sellers.html',
          controller: 'profileCtrl'
      }).
      when('/influencers', {
          templateUrl: 'partials/influencers.html',
          controller: 'profileCtrl'
      }).
      when('/profile', {
          templateUrl: 'partials/profile.html',
          controller: 'profileCtrl'
      }).

      when('/board', {
          templateUrl: 'partials/board.html',
          controller: 'boardCtrl'
      }).
      when('/explore', {
          templateUrl: 'partials/explore.html',
          controller: 'exploreCtrl',
          resolve: {
              user: function($q, User) {
                  var deferred = $q.defer();
                  var callback = function(result) {
                      deferred.resolve(result);
                  };

                  User.getAuthenticatedUser(callback);
                  return deferred.promise;
              },
              // the masonry layout breaks when $resource returns data (changes the height of the elements),
              // so everything is done here. Should be moved into service.
              advisors: function($q, User, $http, $sce) {
                  var choose_block = function(index) {
                      if (index == 0)
                          return {"template": 'detail', "length": 1};

                      var template = ['horizontal', 'vertical', 'horizontal-detail'][Math.floor(Math.random() * 3)];

                      switch(template) {
                          case 'horizontal':
                              var length = 2;
                              break;
                          case 'vertical':
                              var length = 5;
                              break;
                          default:
                              var length = 1;
                      }

                      return {
                          "template": template,
                          "length": length
                      }
                  }

                  var deferred = $q.defer();
                  var callback = function(result) {
                      var user = User.getAuthenticatedUser(function(val, response) {
                          result = result.filter(function(item) { return item.title == 'FOLLOWING' });
                          var promises = result.map(function(item) { 
                              // TODO: send params instead of hardcoding
                              return $http.post('/web/agent/' + item.id + '/items?count=5', {"id": val._id});
                          });

                          $q.all(promises).then(function(data) { 
                              for (var i = 0; i < result.length; i++) {
                                  var products = data[i].data;
                                  var advisor = result[i];
                                  advisor.products = products;
                                  advisor.block = choose_block(i);
                                  advisor.current_products = products.slice(0, advisor.block.length);
                                  advisor.block.trustedTemplate = $sce.trustAsResourceUrl(advisor.block.template + ".html");
                                  
                                  products.map(function(item) { 
                                      item.discount = Math.round(100 - item.sources[0].actual_price / item.list_price * 100) || null; 
                                  });
                              }
                              deferred.resolve(result);
                          });
                      });
                  }

                  User.getInfluencers(callback);
                  return deferred.promise;
              }
          }
      }).
      when('/apps', {
          templateUrl: 'partials/apps.html',
          controller: 'appsCtrl',
          resolve: {
              venues: function($q, Venue) {
                  var deferred = $q.defer();
                  var callback = function(result) {
                      deferred.resolve(result);
                  };

                  Venue.getVenues(callback);
                  return deferred.promise;
              }
          }
      }).
      when('/apps/:app_id', {
          templateUrl: 'partials/apps_edit.html',
          controller: 'appsCtrl',
          resolve: {
              venues: function($q, Venue) {
                  var deferred = $q.defer();
                  var callback = function(result) {
                      deferred.resolve(result);
                  };

                  Venue.getVenues(callback);
                  return deferred.promise;
              }
          }
      }).
      when('/boards', {
          templateUrl: 'partials/boards.html',
          controller: 'boardsCtrl'
      }).
      when('/boards/:board_id', {
          templateUrl: 'partials/board_edit.html',
          controller: 'boardsCtrl'
      }).
      when('/about', {
          templateUrl: 'partials/about.html',
          controller: 'appCtrl'
      }).
      when('/tos', {
          templateUrl: 'partials/tos.html',
          controller: 'profileCtrl'
      }).
      when('/beta', {
          templateUrl: 'partials/beta.html',
          controller: 'loginCtrl'
      }).
      when('/testflight', {
          templateUrl: 'partials/testflight.html',
          controller: 'testFlightCtrl'
      }).
      when('/welcome', {
          templateUrl: 'partials/welcome.html'
      }).
      when('/basic_info', {
          templateUrl: 'partials/basic_info.html',
          controller:'userPreferencesController',
          resolve: {
            user: function($q, User) {
              var deferred = $q.defer();
              var callback = function(result) {
                  deferred.resolve(result);
              };
              User.getAuthenticatedUser(callback);
              return deferred.promise;
            }
          }
      }).
      when('/offers', {
          templateUrl: 'partials/offers.html',
          controller: 'offerController',
          resolve: {
              offers: function($q, User) {
                  var deferred = $q.defer();
                  var callback = function(result) {
                      deferred.resolve(result);
                  };

                  User.getOffers(callback);
                  return deferred.promise;
              }
          }
      })
      .when('/offer/:offerId', {
        templateUrl : 'partials/offer.html',
        controller : 'offerController',
        resolve: {
          user: function($q, User) {
              var deferred = $q.defer();
              var callback = function(result) {
                  deferred.resolve(result);
              };
            User.getAuthenticatedUser(callback);
            return deferred.promise;
          },
          offers: function($q, User) {
              var deferred = $q.defer();
              var callback = function(result) {
                  deferred.resolve(result);
              };

            User.getOffers(callback);
            return deferred.promise;
          }
        }

      })
      .when("/recentoffer/user/:userId/offer/:offerId", {
         templateUrl : 'partials/offer.html',
          controller : 'offerController',
          resolve: {
          // user: function($q, User) {
          //     var deferred = $q.defer();
          //     var callback = function(result) {
          //         deferred.resolve(result);
          //     };
          //   User.getAuthenticatedUser(callback);
          //   return deferred.promise;
          // },
          offers: function($q, User) {
              var deferred = $q.defer();
              var callback = function(result) {
                  deferred.resolve(result);
              };

            User.getOffers(callback);
            return deferred.promise;
          }
        }
      })
      .when('/preferences', {
          templateUrl: 'partials/preferences.html'
      });

  //
  // $locationProvider.html5Mode(true);
}).
run(function($rootScope, $location, User) {
    if($location.path().indexOf("/recentoffer") == -1){
      // if($location.path() == "/boards"){
      //   $rootScope.user = User.getAuthenticatedUser(function(value, responseHeaders) {
      //         $rootScope.isAuthenticated = true; //($rootScope.user.is_registered && $rootScope.user.is_approved);
      //         $location.path($location.path());
      //     }, function(errorResponse) {
      //         $location.path('/intro');
      //     });
      // }
      if ($location.path() != '/testflight') {
          $rootScope.user = User.getAuthenticatedUser(function(value, responseHeaders) {
              console.log($rootScope.user);
              $rootScope.isAuthenticated = true; //($rootScope.user.is_registered && $rootScope.user.is_approved);
              if($location.path() == "/apps"){
                $location.path('/apps');
              } else if($rootScope.user.is_influencer){
                $location.path('/boards');
              } else {
                $location.path('/welcome');
              }
          }, function(errorResponse) {
              $location.path('/intro');
          });
      }
    } else {
      $rootScope.isAuthenticated = true;
      $location.path($location.path());
    }
});
