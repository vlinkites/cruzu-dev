'use strict';

/* Filters */

angular.module('cruzu.filters', []).
  filter('category', function () {
    return function (influencers,category) {

        if(category && category != "All"){
            var result = [];
            for(var i=0;i<influencers.length;i++){
                if(influencers[i].category.toLowerCase() == category.toLowerCase())
                    result.push(influencers[i]);
            }
            return result;

        }
        else{
            return influencers;
        }
    }
  }).
  filter('truncate', function() {
    return function(input, maxlength) {

      if (input.length > maxlength + '...'.length) {
        return input.slice(0, maxlength) + '...';
      }

      return input;
    }
  });
