var offerController = angular.module('cruzu.profileController', []);

offerController.controller('profileCtrl', function ($rootScope, $scope, $location, User, Product, Board) {

    $scope.isActive = true;

    $rootScope.user = User.getAuthenticatedUser(
        function(value, responseHeaders){
            $rootScope.isAuthenticated = $rootScope.user.is_registered;
            $scope.isActive = $rootScope.user.is_active;
        },function(errorResponse){
            $location.path('/login');
        }
    );

    $scope.setActive = function(active) {
        $scope.isActive = active;
        User.updateProfile({is_active:active});
    }
});

