var app = angular.module('cruzu');

app.controller('appCtrl', function($scope, User, $location, $route, $anchorScroll) {
    $anchorScroll();

    $scope.currPath = $location.path();
    if ($scope.currPath=='/testflight')
        return;

    //$scope.isAuthenticated = $scope.user.is_registered;

    $scope.largeHeader = function() { return ['/intro', '/about'].indexOf($scope.currPath) != -1; }

    //User.getAuthenticatedUser(function(value) { $scope.user = value; });
    $scope.$on('user:loggedIn', function(event, user) { $scope.user = user; $scope.isAuthenticated = true});
    $scope.$on('$routeChangeSuccess', function() { 
      $scope.currPath = $location.path(); 
      $scope.largeHeader();
    });

    $scope.login = function(){
        var width = 600;
        var height = 400;
        var left = (screen.width/2)-(width/2);
        var top = (screen.height/2)-(height/2);

        var win = window.open('/auth/service/facebook', 'Facebook Login', "menubar=no,toolbar=no,status=no,width="+width+",height="+height+",toolbar=no,left="+left+",top="+top);
        var intervalID = setInterval(function(){
            if (win.closed) {
                clearInterval(intervalID);
                User.getAuthenticatedUser(
                    function(value, responseHeaders){
                        $scope.user = value;
                        $scope.$emit('user:loggedIn', value);
                        var url = $scope.user.is_influencer ? "/boards" : "/welcome";
                        $location.url(url);
                    },function(errorResponse){
                        alert(errorResponse.data.error);
                        window.location = "#/intro";
                    }
                )
            }
        }, 500);
    }
});
