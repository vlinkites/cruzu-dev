'use strict';

var userPreferencesController = angular.module('cruzu.userPreferencesController', []);

userPreferencesController.controller('userPreferencesController', function($scope, $timeout, $location, $http, User, $rootScope, $window) {
	$scope.userPreferencesData = "";
  $scope.currentDisplayTab = "tab1-wizard-custom-circle";
  $scope.previousTab = "";
  $scope.nextTab = "tab2-wizard-custom-circle";
  $scope.currentForm = "";

  $scope.userPreferencesStylePrefs  = function(index, value){
    angular.forEach($scope.userPreferencesData.style_prefs, function(v,i){
      if(i==index){
        if($scope.userPreferencesData.style_prefs[i].value == value) {
          $scope.userPreferencesData.style_prefs[i].value = 0;
          $("#button-style-prefs-"+i+"-"+value).blur();
          
          if(value == 1){
            $("#button-style-prefs-"+i+"-"+value).css("color", "#bf4346")
            $("#button-style-prefs-"+i+"-"+value).css("background", "none");
            $("#button-style-prefs-"+i+"-"+value).css("border","1px solid #bf4346");
          } else if(value == 2){
            $("#button-style-prefs-"+i+"-"+value).css("color", "#f2994b")
            $("#button-style-prefs-"+i+"-"+value).css("background", "none");
            $("#button-style-prefs-"+i+"-"+value).css("border","1px solid #f2994b");
          }else if(value == 3){
            $("#button-style-prefs-"+i+"-"+value).css("color", "#488c6c")
            $("#button-style-prefs-"+i+"-"+value).css("background", "none");
            $("#button-style-prefs-"+i+"-"+value).css("border","1px solid #488c6c");
          } else {
            $("#button-style-prefs-"+i+"-"+value).blur();
            $("#button-style-prefs-"+i+"-"+value).removeAttr("style");
          }

          $("#button-style-prefs-"+i+"-"+value).bind("mouseout", function(){
            $("#button-style-prefs-"+i+"-"+value).blur();
            $("#button-style-prefs-"+i+"-"+value).removeAttr("style");
          });
        }
        else{
          $scope.userPreferencesData.style_prefs[i].value = value;
          $("#button-style-prefs-"+i+"-"+value).removeAttr("style");
        }
      }
    });
  }

  $scope.getUserPreferences = function(){
  	$http.get('/user/'+$scope.user._id+'/preferences')
  	.success(function(data){
  		$scope.userPreferencesData = data;
  	})
  	.error(function(error){
  		console.log(error);
  	});
  }

  $scope.saveUserPreferences = function(){
    console.log($scope.userPreferencesData);

    var array = new Array();
    angular.forEach($scope.userPreferencesData.basic, function(value, index){
      var obj = new Object();
      obj.code = value.code;
      obj.value = value.value;
      array.push(obj);
    });
    angular.forEach($scope.userPreferencesData.style_prefs, function(value, index){
      var obj = new Object();
      obj.code = value.code;
      obj.value = value.value;
      array.push(obj);
    });

    angular.forEach($scope.userPreferencesData.offer_prefs, function(value, index){
      var obj = new Object();
      obj.code = value.code;
      obj.value = value.value;
      array.push(obj);
    });

    $http.post('/user/'+$scope.user._id+'/preferences', {preferences : array})
    .success(function(data){
      console.log(data);
      $window.location.href = '#/intro';
    })
    .error(function(error){
      console.log(error);
    })
  }

  $scope.initSelectValue = function(index, value){
    angular.forEach($scope.userPreferencesData.basic, function(val, ind){
      if(ind == index){
        if(isNaN(value)){
          $scope.userPreferencesData.basic[index].value = "select";
        }
      }
    });
  }

  $scope.getRange = function(num) {
  	console.log(num, new Array(num));
    return new Array(num);   
  }

  $scope.setCurrentTabAndForm = function(currentTab){

    var approved = true;

    if(currentTab == "tab1-wizard-custom-circle"){

    } else if(currentTab == "tab2-wizard-custom-circle") {

      angular.forEach($scope.userPreferencesData.basic, function(value, index){
        if($("#form-tab1-wizard-custom-circle-"+value.code).val() == ""){
          $("#form-tab1-wizard-custom-circle-"+value.code).parent().parent().find("i.alert").removeClass("alert-hide");
        } else {
           $("#form-tab1-wizard-custom-circle-"+value.code).parent().parent().find("i.alert").addClass("alert-hide");
           approved = false;
        }
      });
    }
    if(!approved && currentTab == "tab2-wizard-custom-circle"){
      $("#tab2-wizard-custom-circle").removeClass("active");
      $("#tab3-wizard-custom-circle").removeClass("active");
      $("#tab4-wizard-custom-circle").removeClass("active");
      $("#tab1-wizard-custom-circle").addClass("active");
      return false;
    } else if(approved && currentTab == "tab2-wizard-custom-circle") {
      setTimeout(function(){
      $scope.$apply(function() {
      $scope.currentDisplayTab = currentTab;
      
      $scope.currentForm = "form-"+currentTab;
      if($scope.currentDisplayTab == "tab1-wizard-custom-circle"){
        $scope.previousTab = "";
        $scope.nextTab = "tab2-wizard-custom-circle"
      } else if($scope.currentDisplayTab == "tab2-wizard-custom-circle"){
        $scope.previousTab = "tab1-wizard-custom-circle";
        $scope.nextTab = "tab3-wizard-custom-circle";
      } else if($scope.currentDisplayTab == "tab3-wizard-custom-circle"){
        $scope.previousTab = "tab2-wizard-custom-circle";
        $scope.nextTab = "tab4-wizard-custom-circle";
      } else if($scope.currentDisplayTab == "tab4-wizard-custom-circle"){
        $scope.previousTab = "tab3-wizard-custom-circle";
        $scope.nextTab = "";
      } else {
        $scope.previousTab = "";
        $scope.nextTab = "";
      }
      console.log($scope.currentDisplayTab);
      console.log($scope.previousTab);
      console.log($scope.nextTab);
    });
    }, 100);
    }
  }


});
