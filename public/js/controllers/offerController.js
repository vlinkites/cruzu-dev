'use strict';
var offerController = angular.module('cruzu.offerController', []);

offerController.controller('offerController', function ($rootScope, $http, $routeParams, $scope, $location, $anchorScroll, User, offers) {
    $scope.offerProducts = new Array();
    $scope.offers = new Array();
    $scope.offerSource = "";
    $scope.offerTotals = "";
    $scope.buyLink = "";
    var monthArray = ["January","February","March","April","May","June","July","August","September","October","November","December"];

    var dateObj = new Date();
    $scope.offerDate = monthArray[dateObj.getMonth()] + " " + dateObj.getDate() + ", " + dateObj.getFullYear();
     // $rootScope.user = User.getAuthenticatedUser(
     //     function(value, responseHeaders){
     //         $rootScope.isAuthenticated = $rootScope.user.is_registered;
     //         $scope.hide_offer_msg = $rootScope.user.hide_offer_msg;
     //         $scope.notification = {text: "These are sample offers created by your team. You'll soon start receiving these types of offers via email."};

     //       //  $location.hash('top');
     //       //  $anchorScroll();

     //     },function(errorResponse){
     //         $location.path('/login');
     //     }
     // )
    $scope.getOffers = function(){
        $http.get("/user/"+$scope.user._id+"/offer")
        .success(function(data){
            if(data) $scope.offers.push(data);
        })
        .error(function(data){
            console.log(error);
        });
    }

    $scope.getOffer = function(){

        var url = null;
        if($scope.user) url = "/user/"+$scope.user._id+"/offer/"+$routeParams.offerId;
        else if($routeParams.userId) {url = "/user/"+$routeParams.userId+"/offer/"+$routeParams.offerId;}
       
        $http.get(url)
        .success(function(data){
            $scope.offer = data;
            $scope.offerProducts = data.products;
            $scope.offerSource = data.source;
            $scope.offerTotals = data.totals;
            $scope.buyLink = data.buy_link;

            setTimeout(function(){
                var circles = [];
                angular.forEach($scope.offerProducts, function(value, index){
                    var child = document.getElementById('circles-'+value.product_id+"-"+index);
                    var percentage = value.score;
                    if(child && percentage){
                        circles.push(Circles.create({
                            id:         child.id,
                            value:      percentage,
                            radius:     75,
                            width:      15,
                            colors:     ['#e4e4e4', '#930b0b']
                        }));
                    }
                });
            }, 100);

            var x = 1;
            function slideImage(){
                setTimeout(function(){
                    angular.forEach($scope.offerProducts, function(value, index){
                        //console.log('img-'+value.product_id+"-"+index, value.sensory_images[parseInt(x%value.sensory_images.length)]);
                        //$('#img-'+value.product_id+"-"+index).html("<img src='"+value.sensory_images[parseInt(x%value.sensory_images.length)]+"' alt='' />").fadeIn("slow");
                        $("#img-"+value.product_id+"-"+index).css("background-image","url("+value.sensory_images[parseInt(x%value.sensory_images.length)]+")");
                    });
                    x++;
                    slideImage();
                }, 5000);
            }
            setTimeout(function(){
                angular.forEach($scope.offerProducts, function(value, index){
                    //console.log('img-'+value.product_id+"-"+index, value.sensory_images[parseInt(x%value.sensory_images.length)]);
                    $("#img-"+value.product_id+"-"+index).css("background-image","url("+value.sensory_images[parseInt(x%value.sensory_images.length)]+")");
                });
                x++;
                slideImage();
            }, 200);
            //slideImage();
        })
        .error(function(error){
            console.log(error);
        })
    }

    $scope.initMap = function(lat,lng){

        var myLatlng = new google.maps.LatLng(lat ,lng);

         var mapOptions = {
            zoom: 12,
            center: myLatlng
          };

          var map = new google.maps.Map(document.getElementById('map-canvas'),
              mapOptions);
          console.log(map);
          var marker = new google.maps.Marker({
              position: myLatlng,
              map: map,
              title: 'Hello World!'
          });
          marker.setMap(map);
    }

});
