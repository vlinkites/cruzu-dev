var offerDetailsController = angular.module('cruzu.offerDetailsController', []);

offerDetailsController.controller('offerDetailsCtrl', function ($scope, $modal, $location, $routeParams, User, offer) {

    $scope.offer = offer;

    // $rootScope.user = User.getAuthenticatedUser(
    //     function (value, responseHeaders) {
    //         $rootScope.isAuthenticated = $rootScope.user.is_registered;

    //         // $scope.offer = User.getOfferDetails({id: $routeParams.offer_id}, function (value, responseHeaders) {
    //         //     $scope.sourceMsg = ($scope.offer.sources.length == 1) ? "" : ($scope.offer.sources.length == 2) ?
    //         //                         "Also at "+$scope.offer.sources[1].name : "see all "+$scope.offer.sources.length+" sources";
    //         // }, function(errorResponse) {
    //         //     console.log("Error retrieving offer details");
    //         // });
    //     }, function (errorResponse) {
    //         $location.path('/login');
    //     }
    // );

    $scope.save_click = function () {

        User.saveClick({offer_id: $scope.offer.id, source_id: $scope.offer.sources.length > 0 ? $scope.offer.sources[0].source_id : ""},
            function (value, responseHeaders) {
                var win = window.open($scope.offer.sources[0].url, '_self');
            }, function (errorResponse) {
                alert(errorResponse.data.error);
            })
    };

    $scope.showFeedbackModal = function () {

        modalInstance = $scope.offerModal = $modal.open({
            templateUrl: 'feedbackModal.html',
            controller: 'offerModalCtrl',
            resolve: {offer_id : function() {return $scope.offer.id }}
        });
    };
});



var offerModalCtrl = function ($scope, User, offer_id) {

   $scope.feedback = {};
   $scope.feedback.content = "";

    // Send the user feedback back to server
    $scope.ok = function () {
        $scope.msg = "Thanks for the feedback!"
        setTimeout(function(){$scope.close()},2000);
        User.sendFeedback({id:offer_id, feedback:$scope.feedback.content});
    };

    $scope.close = function () {
        modalInstance.close();
    };

};


offerDetailsController.controller('offerModalCtrl', offerModalCtrl);


