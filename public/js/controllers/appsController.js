var app = angular.module('cruzu.appsController', []);

app.controller('appsCtrl', function ($scope, $routeParams, $location, $modal, $q, $http, App, User, Agent, venues) {

    $scope.loading = true;
    $scope.venues = venues;

    $scope.options = {
        types: [
            {value: 'image',      display: 'Image Card'},
            {value: 'content',    display: 'Content Card'},
            {value: 'agent',      display: 'Advisor Card'},
            {value: 'navigation', display: 'Navigation Card'}
        ],
        sizes: [
            {value: 'small',  display: 'Small Size'},
            {value: 'medium', display: 'Medium Size'},
            {value: 'large',  display: 'Large Size'}
        ],
        links: [
            {value: 'page',  display: 'Page'},
            {value: 'agent', display: 'Agent'}
        ]
    }
    $scope.card_types = [
    ];

    $scope.card_sizes = [
        {value: 'image',      display: 'Image Card'},
        {value: 'content',    display: 'Content Card'},
        {value: 'agent',      display: 'Advisor Card'},
        {value: 'navigation', display: 'Navigation Card'}
    ];

    var fix_data_format = function() {
        for (var i = 0; i < $scope.apps.length; i++) {
            for (var j = 0; j < $scope.apps[i].pages.length; j++) {
                for (var k = 0; k < $scope.apps[i].pages[j].block_list.length; k++) {
                    if (!$scope.apps[i].pages[j].block_list[k].item_list)
                        $scope.apps[i].pages[j].block_list[k].item_list = [];
                    if (!$scope.apps[i].pages[j].block_list[k].item_list.length) {
                        $scope.apps[i].pages[j].block_list[k].item_list.push({});
                        save_app($scope.apps[i]);
                    }
                }
            }
        }
    }

    var save_app = function(app) {
        App.update({app_id: $scope.app._id, app: $scope.app}, function() {
            console.log('fixed');
        });
    }

    var venue_name = function(id) {
        return $scope.venues.filter(function(item) { return item._id === id; })[0].name;
    }

    App.getApps(function(val) {
        $scope.apps = val;
        $scope.app = $scope.apps.filter(function(item) { return item._id === $routeParams.app_id })[0];
        if ($scope.app)
            $scope.tags = $scope.app.target_venues.map(function(venue) {return {name: venue_name(venue), value: venue}});
        $scope.loading = false;
    });

    User.getInfluencers(function(val) {
        $scope.advisors = val;
    });

    $scope.add_app = function() {
        App.newApp(function(value) {
            $scope.app = value;
            $scope.app.hide = true;
            $scope.apps.push($scope.app);
            $scope.app.owner_id.push($scope.user._id);
            $scope.add_page();
            $scope.save();
            $location.url('/apps/' + value._id);
        });
    };

    $scope.delete_app = function() {
        App.deleteApp({app_id: $scope.app._id});
        $location.url('/apps');
    };

    $scope.add_page = function() {

        var page = {block_list: [], page_code : ObjectId()};
        $scope.add_card(page);
        $scope.app.pages.push(page);
    };

    $scope.delete_page = function(index) {
        $scope.app.pages.splice(index, 1);
    };

    $scope.add_card = function(page) {
        var card = {item_list: []};
        $scope.add_item(card);
        page.block_list.push(card);
    };

    $scope.delete_card = function(page, index) {
        page.block_list.splice(index, 1)
    };

    $scope.add_item = function(card) {
        card.item_list.push({});
    };
    
    $scope.delete_item = function(card, index) {
        card.item_list.splice(index, 1)
    };

    $scope.save = function() {
        $scope.loading = true;
        if ($scope.tags)
            $scope.app.target_venues = $scope.tags.map(function(item) { return item.value });
        App.update({app_id: $scope.app._id, app: $scope.app}, function() {
            $scope.loading = false;
        });
    };

    $scope.move_up = function(page, index) {
        var page_index = $scope.app.pages.indexOf(page);
        $scope.app.pages[page_index].block_list.splice(index - 1, 0, $scope.app.pages[page_index].block_list.splice(index, 1)[0]);
    };

    $scope.move_down = function(page, index) {
        var page_index = $scope.app.pages.indexOf(page);
        $scope.app.pages[page_index].block_list.splice(index + 1, 0, $scope.app.pages[page_index].block_list.splice(index, 1)[0]);
    };

    $scope.range = function(max) {
        return new Array(max);
    };

    $scope.pick_file = function(model, attr) {
        filepicker.pickAndStore({}, {}, function(data) {
            $scope.$apply(function() {
                model[attr] = data[0].url;
            });
        });
    };

    $scope.boards = {};
    $scope.get_boards = function(agent_id) { 
        if(agent_id) {
            Agent.getBoards({id: agent_id}, function(value) {
                $scope.boards[agent_id] = value;
            });
        }
    };

    $scope.delete_selected_values = function(card) {
        delete card.selected_values;
    };

    $scope.questions_modal = function(agent_id, board_id, page, card_index) {
        var agent = $scope.advisors.filter(function(a) { return a.id == agent_id; })[0];
        var board = $scope.boards[agent_id].filter(function(b) { return b._id == board_id; })[0];
        var page_index = $scope.app.pages.indexOf(page);
        var modalInstance = $modal.open({
            templateUrl: 'partials/default_agent_questions.html',
            controller: defaultAgentQuestionsCtrl,
            resolve: {
                app:   function() { return $scope.app; },
                board: function() { return board; },
                agent: function() { return agent; },
                page:  function() { return page_index; },
                card:  function() { return card_index; }
            }
        });
    };

    $scope.beacon_modal = function(page_index) {
        var modalInstance = $modal.open({
            templateUrl: 'beacon_modal.html',
            controller: 'beaconCtrl',
            resolve: {
                app: function() { return $scope.app; },
                page: function() { return page_index; }
            }
        });
    };
});

var beaconCtrl = function($scope, $modalInstance, App, app, page) {
    $scope.app = app;
    $scope.page = page;
    if (!$scope.app.pages[page].trigger_beacon)
        $scope.app.pages[page].trigger_beacon = {};

    $scope.save = function() {
        App.update({app_id: $scope.app._id, app: $scope.app}, function() {
            $modalInstance.close();
        });
    };

    $scope.close = function() {
        $modalInstance.dismiss('close');
    };
}

var defaultAgentQuestionsCtrl = function($scope, $modalInstance, App, app, board, agent, page, card) {
    $scope.agent = agent;
    $scope.board = board;
    $scope.app   = app;
    $scope.page  = page;
    $scope.card  = card;

    var initialize_selected_values = function() {
        var selected_values = $scope.app.pages[page].block_list[card].selected_values;

        angular.forEach(selected_values, function(value) {
            if (value.return_value && typeof value.return_value[0] === 'object') delete value.return_value; // legacy data reset

            var text = value.return_value && !value.display_value;
            var checkbox = Array.isArray(value.return_value);

            if (text) {
                value._value = value.return_value;
            } 
            else if (checkbox) {
                value._value = [];
                for (var i = 0; i < value.return_value.length; i++) {
                    value._value.push({return_value: value.return_value[i], display_value: value.display_value[i]});
                }
            }
            else {
                value._value = {};
                value._value.return_value  = value.return_value;
                value._value.display_value = value.display_value;
            }
        });
    };

    var set_selected_values = function() {
        var selected_values = $scope.app.pages[page].block_list[card].selected_values;
        for(var i = 0; i < selected_values.length; i++) {
            if (!selected_values[i]._value) {
                selected_values.splice(i, 1);
            }
        };
        angular.forEach($scope.app.pages[page].block_list[card].selected_values, function(value) {
            var text = typeof value._value === 'string';
            var checkbox = Array.isArray(value._value);

            if (text) {
                value.return_value = value._value
            }
            else if (checkbox) {
                value.return_value  = value._value.map(function(item) { return item.return_value; });
                value.display_value = value._value.map(function(item) { return item.display_value; });
            }
            else {
                value.return_value  = value._value.return_value;
                value.display_value = value._value.display_value;
            }
            
            delete value._value;
        });
    };


    if (!$scope.app.pages[page].block_list[card].selected_values) {
        $scope.app.pages[page].block_list[card].selected_values = [];
    } else {
        initialize_selected_values();
    }

    if($scope.board.questions.length != $scope.app.pages[page].block_list[card].selected_values.length) {
        $scope.board.questions.map(function(item) {
            var found;
            for (var i = 0; i < $scope.app.pages[page].block_list[card].selected_values.length; i++) {
                var selected_value = $scope.app.pages[page].block_list[card].selected_values[i];
                if (item.code === selected_value.code) {
                    found = true;
                }
            }

            if (!found)
              $scope.app.pages[page].block_list[card].selected_values.push({board_id: $scope.board._id, code: item.code, _value: []});
        });
    }


    $scope.selected_value = function(question) {
        var selected_values = $scope.app.pages[page].block_list[card].selected_values;
        for (var i = 0; i < selected_values.length; i++) {
            if (question.code === selected_values[i].code && selected_values[i].board_id === $scope.board._id) {
                return i;
            }
        }
    };

    $scope.ok = function() {
        set_selected_values();
        App.update({app_id: $scope.app._id, app: $scope.app}, function() {
            $modalInstance.close();
        });
    };

    $scope.cancel = function() { $modalInstance.dismiss('cancel'); }

};


var ObjectId = (function () {
    var increment = 0;
    var pid = Math.floor(Math.random() * (32767));
    var machine = Math.floor(Math.random() * (16777216));

    if (typeof (localStorage) != 'undefined') {
        var mongoMachineId = parseInt(localStorage['mongoMachineId']);
        if (mongoMachineId >= 0 && mongoMachineId <= 16777215) {
            machine = Math.floor(localStorage['mongoMachineId']);
        }
        // Just always stick the value in.
        localStorage['mongoMachineId'] = machine;
        document.cookie = 'mongoMachineId=' + machine + ';expires=Tue, 19 Jan 2038 05:00:00 GMT'
    }
    else {
        var cookieList = document.cookie.split('; ');
        for (var i in cookieList) {
            var cookie = cookieList[i].split('=');
            if (cookie[0] == 'mongoMachineId' && cookie[1] >= 0 && cookie[1] <= 16777215) {
                machine = cookie[1];
                break;
            }
        }
        document.cookie = 'mongoMachineId=' + machine + ';expires=Tue, 19 Jan 2038 05:00:00 GMT';

    }

    function ObjId() {
        if (!(this instanceof ObjectId)) {
            return new ObjectId(arguments[0], arguments[1], arguments[2], arguments[3]).toString();
        }

        if (typeof (arguments[0]) == 'object') {
            this.timestamp = arguments[0].timestamp;
            this.machine = arguments[0].machine;
            this.pid = arguments[0].pid;
            this.increment = arguments[0].increment;
        }
        else if (typeof (arguments[0]) == 'string' && arguments[0].length == 24) {
            this.timestamp = Number('0x' + arguments[0].substr(0, 8)),
                this.machine = Number('0x' + arguments[0].substr(8, 6)),
                this.pid = Number('0x' + arguments[0].substr(14, 4)),
                this.increment = Number('0x' + arguments[0].substr(18, 6))
        }
        else if (arguments.length == 4 && arguments[0] != null) {
            this.timestamp = arguments[0];
            this.machine = arguments[1];
            this.pid = arguments[2];
            this.increment = arguments[3];
        }
        else {
            this.timestamp = Math.floor(new Date().valueOf() / 1000);
            this.machine = machine;
            this.pid = pid;
            this.increment = increment++;
            if (increment > 0xffffff) {
                increment = 0;
            }
        }
    };
    return ObjId;
})();

ObjectId.prototype.getDate = function () {
    return new Date(this.timestamp * 1000);
};

ObjectId.prototype.toArray = function () {
    var strOid = this.toString();
    var array = [];
    var i;
    for(i = 0; i < 12; i++) {
        array[i] = parseInt(strOid.slice(i*2, i*2+2), 16);
    }
    return array;
};

/**
 * Turns a WCF representation of a BSON ObjectId into a 24 character string representation.
 */
ObjectId.prototype.toString = function () {
    var timestamp = this.timestamp.toString(16);
    var machine = this.machine.toString(16);
    var pid = this.pid.toString(16);
    var increment = this.increment.toString(16);
    return '00000000'.substr(0, 8 - timestamp.length) + timestamp +
        '000000'.substr(0, 6 - machine.length) + machine +
        '0000'.substr(0, 4 - pid.length) + pid +
        '000000'.substr(0, 6 - increment.length) + increment;
};
