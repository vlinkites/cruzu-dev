var offerController = angular.module('cruzu.boardController', []);

offerController.controller('boardCtrl', function ($rootScope, $scope, $location, User, Product, Board) {

    $scope.showFilter = false;
    $scope.isActive = true;
    $scope.isInfluencerActive = $rootScope.user.is_influencer_active;
    $scope.currProductVintage = null;

    $rootScope.user = User.getAuthenticatedUser(
        function(value, responseHeaders){
            $rootScope.isAuthenticated = $rootScope.user.is_registered;
            $scope.isActive = $rootScope.user.is_active;
        },function(errorResponse){
            $location.path('/login');
        }
    )

    $scope.setActive = function(active) {

        $scope.isActive = active;
        User.updateProfile({is_active:active});

    }

    $scope.setInfluencerActive = function(active) {

        $scope.isInfluencerActive = active;
        User.updateProfile({is_influencer_active:active});

    }

    $scope.viewProducts = function(){
        $scope.products = Product.getProducts({id: $scope.query});
    }

    $scope.vintageClick = function(index){
        $scope.products[index].vintage.lowest_price ? $scope.products[index].price = "$" + $scope.products[index].vintage.lowest_price : "";
        $scope.products[index].vintage.highest_price ? $scope.products[index].price += " - $" + $scope.products[index].vintage.highest_price : "";
        $scope.products[index].isAdded = $scope.products[index].vintage.isAdded;
        $scope.currProductVintage = $scope.products[index].vintage;
    }

    $scope.addProduct = function(index){
        Product.addProduct({product: $scope.products[index]},
            function (value, responseHeaders) {
                $scope.wines = Product.getWines();
                $scope.products[index].isAdded = true;
                $scope.products[index].vintage.isAdded = true;
            }, function (errorResponse) {
                alert(errorResponse.data.error);
            });
    }

    $scope.wines = Product.getWines();

    $scope.saveWines = function(currWine){

        Board.updateProducts({products:[currWine]},
            function (value, responseHeaders) {
            }, function (errorResponse) {
                alert(errorResponse.data.error);
            })
    }

    $scope.deleteWine = function(index){
        Board.deleteProducts({products: $scope.wines.slice(index,1)},
            function (value, responseHeaders) {
                $scope.wines.splice(index,1);
            }, function (errorResponse) {
                alert(errorResponse.data.error);
            })
    }


    $scope.deleteWines = function(){
        var data = getSelectedWines($scope.wines);
        Board.deleteProducts({products: data.wines},
            function (value, responseHeaders) {
                for(var i=0; i<data.indexes.length;i++){
                    $scope.wines.splice(data.indexes[i],1);
                }
            }, function (errorResponse) {
                alert(errorResponse.data.error);
            })
    }

    $scope.saveBio = function(){
        User.updateProfile({short_bio:$scope.user.short_bio, long_bio:$scope.user.long_bio})
    }

    $scope.formattedSource = function() {
        if (!$scope.currProductVintage)
            return "";

        var results = "";
        $scope.currProductVintage.sources.forEach(function(source) {
            results += source.name + " ("+source.price+")\n";
        });
        return results;
    };

    $scope.showSearch = function(active) {
        $scope.showFilter = active;
    };

});

// Not currently used
function getSelectedWines(wines){
    var toReturn = [];
    var selectedWines = [];
    for(var i=0; i<wines.length;i++){
        if(wines[i].selected){
            toReturn.push(wines[i]);
            selectedWines.push(i);
        }

    }

    return {
        wines: toReturn,
        indexes: selectedWines
    };


}

