'use strict';

var generalQuestionController = angular.module('cruzu.generalQuestionController', []);

generalQuestionController.controller('generalQuestionCtrl', function ($rootScope, $scope, $location, $anchorScroll, $modal, $http, User, controls) {

    $scope.inactive = function() {
        return $http.pendingRequests.length > 0;
    };

    $scope.controls = controls;
    $scope.disabled = false;

    //$location.hash('top');
    //$anchorScroll();

    // initialize answers to fetch/assign previously selected answers.
    // If user has no answers, default to display the first option.
    if ($scope.user.profile_answers && $scope.user.profile_answers[0]) {
      $scope.answers = $scope.user.profile_answers
    }

    for (var i = 0; i < $scope.controls.length; i++) {
      if ($scope.answers && $scope.answers[i]) {
        $scope.controls[i].answer = $scope.answers.filter(function(item) { return item.id == $scope.controls[i].id })[0];
      } else if ($scope.controls[i].type == 'dropdown') {
        $scope.controls[i].answer = $scope.controls[i].options[0];
      }
    }
    console.log(JSON.stringify($scope.controls,null,4));

    // SAVE GENERAL ANSWERS (CLICKED OK ON MODAL)
    $scope.ok = function () {
        $scope.disabled = true;

        console.log(JSON.stringify($scope.controls,null,4));

        // Set return_values based on user's selected value (which is the display_value). If there is an option list for the question
        // then go through those options until we find the one that matches the user selection and grab return_value from there
        for (var i=0;i < $scope.controls.length;i++) {
            if (!$scope.controls[i].options)     // Default to same value for "non-option" questions
                $scope.controls[i].return_value = $scope.controls[i].answer;

          //  if ($scope.controls[i].options) {
          //      for (var j=0; j < $scope.controls[i].options.length; j++) {
          //          if ($scope.controls[i].answer == $scope.controls[i].options[j].value)
          //              $scope.controls[i].return_value = $scope.controls[i].options[j].return_value;
          //      }
          //  }

        }

        User.saveGeneralQuestion({controls: $scope.controls},
            function (value, responseHeaders) {
                $scope.disabled = false;

                if ($rootScope.isAuthenticated) {
                    $location.path('/main');
                }
                else {
                    var modalInstance = $modal.open({
                        templateUrl: 'confirmationModal.html',
                        controller: generalQuestionModalCtrl
                    });

                    modalInstance.result.then(function () {
                        $rootScope.user.is_registered = true;
                        $rootScope.isAuthenticated = true;
                        $location.path('/offers');
                    });
                }

            }, function (errorResponse) {
                alert(errorResponse.data.error);
            })
    };

    $scope.getControlType = function (index) {
        return $scope.controls[index].type;
    }
});

