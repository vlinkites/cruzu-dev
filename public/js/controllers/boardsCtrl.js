var app = angular.module('cruzu.boardsController', []);

app.controller('boardsCtrl', function ($scope, $routeParams, $http, $location, $modal, User, Agent, Question, Board, Attribute, Product) {

    $scope.loading = true

    $scope.pagination = {};
    $scope.pagination.currentPage = 1;
    $scope.setPage = function(page) {
        $scope.pagination.currentPage = page || 1;
        var from = ($scope.pagination.currentPage - 1) * 10;
        var to = from + 10;
        $scope.current_products = $scope.products.slice(from, to);
    };

    var init = function() {
        $scope.board.questions = $scope.board.questions.map(function(question) { return question._id });
        if (!$scope.board.sections.length)
            $scope.board.sections.push({});
        angular.forEach($scope.board.sections, function(section) {
            section.predicate = section.predicate || [];
            angular.forEach($scope.attributes, function(attr) {
                var index = $scope.attributes.indexOf(attr);
                if (!section.predicate[index] && attr.type === 'string')
                    section.predicate.push({attribute: attr.attribute, value: []}); 
                else if (!section.predicate[index] && attr.type === 'range')
                    section.predicate.push({attribute: attr.attribute, value: [null, null]});
            });

            $scope.get_products(section);
        });
    }

    User.getAuthenticatedUser(function(value) {
        $scope.user = value;
        $scope.isAuthenticated = $scope.user.is_registered;

        Agent.getBoards({id: $scope.user._id}, function(val) {
            $scope.boards = val;
            $scope.board = $scope.boards.filter(function(item) { return item._id === $routeParams.board_id })[0];
            Attribute.getAll(function(value) {
                $scope.loading = false;
                $scope.attributes = value;
                if ($scope.board)
                    init();
                    $scope.attribute_view();
            });
        });
    });

    Question.getAll(function(value) {
        $scope.questions = value;
    });

    $scope.get_products = function(section) {
        $http.post('/query/adhoc', {predicate: section.predicate}).success(function(res) {
            $scope.products = res;
            $scope.pagination.totalItems = $scope.products.length;
            $scope.setPage(1);
        })
    };

    $scope.search_view = function() {
        $scope.view = 'search';
        $scope.selected_attribute = false;
    };

    $scope.attribute_view = function() {
        $scope.view = 'attributes';
        $scope.selected_attribute = $scope.attributes[0];
    };

    $scope.select_attribute = function(attribute) {
        $scope.selected_attribute = attribute;
    };

    // TODO: exract
    $scope.pick_file = function(model, attr) {
        filepicker.pickAndStore({}, {}, function(data) {
            $scope.$apply(function() {
                model[attr] = data[0].url;
            });
        });
    };

    $scope.attribute_index = function(attr, section) {
        obj = section.predicate.filter(function(item) { return item.attribute === attr.attribute; })[0];
        return section.predicate.indexOf(obj);
    };

    $scope.add_board = function() {
        $http.post('/board/new').success(function(res) {
            $scope.add_section(res.sections);
            $scope.board = res;
            $scope.board.hide = true;
            $scope.boards.push($scope.board);
            $scope.board.owner_id = $scope.user._id;
            $scope.save_board();
            $location.url('/boards/' + res._id);
        });
    };

    $scope.delete_board = function() {
        Board.deleteBoard({id: $scope.board._id});
        $location.url('/boards');
    };

    $scope.add_section = function(array) {
        var sections = array || $scope.board.sections;
        sections.push({});
        var section = sections[sections.length - 1];

        section.products = [];
        section.predicate = [];
        angular.forEach($scope.attributes, function(attr) {
            if (attr.type === 'string')
                section.predicate.push({attribute: attr.attribute, value: []}); 
            else
                section.predicate.push({attribute: attr.attribute, value: [null, null]});
        });
    };

    $scope.delete_section = function(index) {
        $scope.board.sections.splice(index, 1);
    };

    $scope.add_question = function() {
        $scope.board.questions.push("");
    };

    $scope.delete_question = function(index) {
        $scope.board.questions.splice(index, 1);
    };

    $scope.save_user = function() {
        $scope.loading = true;

        var user = {
            name: $scope.user.name, 
            image: $scope.user.image, 
            long_bio: $scope.user.long_bio, 
            short_bio: $scope.user.short_bio
        };

        User.updateProfile(user, function(value) {
            $scope.loading = false;
        });
    };

    $scope.save_board = function() {
        $scope.loading = true;
        $http.post('/board/' + $scope.board._id, $scope.board).success(function(res) {
            $scope.loading = false;
        });
    }

    // TODO: extract
    $scope.quicksearch = function(value) {
        return $http.get('/product/' + value + '/list_active').then(function(res) {
            var products = [];
            angular.forEach(res.data, function(item) {
                products.push(item);
            });
            return products;
        });
    };

    $scope.question_modal = function(question_id) {
        var question = $scope.questions.filter(function(q) { return q._id === question_id; })[0]
        var modalInstance = $modal.open({
            windowClass: 'single-question-modal',
            templateUrl: 'partials/single_question.html',
            controller: questionCtrl,
            resolve: {
                question: function() { return question; }
            }
        });
    };

    $scope.open = function(data) {
        var productDescription = $modal.open({
            windowClass: 'product-detail-modal description',
            templateUrl: 'partials/product-detail.html',
            controller: productDetailCtrl,
            resolve: {
                data: function() { return data }
            }
        });
    };
});

var questionCtrl = function($scope, $modalInstance, question) {
    $scope.question = question;
}
