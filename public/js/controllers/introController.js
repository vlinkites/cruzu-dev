var app = angular.module('cruzu');

app.controller('introCtrl', function($scope, $timeout, $location, $http, User) {

  $scope.login_form = {};
  $scope.request_link = {};
  $scope.subscribe_text = false;

  $scope.show_login_form = function(type) {
      $scope.request_link[type] = true;
      $scope.login_form[type]   = true;
  };

  $scope.subscribe = function(type) {
      $http.post('/beta/request_beta_access?email=' + $scope.email).success(function() {
          $scope.subscribe_text = true;
          $timeout(function() { 
              $scope.login_form[type] = false; 
              $scope.subscribe_text = false;
          }, 2000);
      });
  };
});
