'use strict';

var loginController = angular.module('cruzu.loginController', []);

loginController.controller('loginCtrl', function ($scope, $location, User) {

    $scope.login = function(){
        var width = 600;
        var height = 400;
        var left = (screen.width/2)-(width/2);
        var top = (screen.height/2)-(height/2);

        var win = window.open('/auth/service/facebook', 'Facebook Login', "menubar=no,toolbar=no,status=no,width="+width+",height="+height+",toolbar=no,left="+left+",top="+top);
        var intervalID = setInterval(function(){
            if (win.closed) {
                clearInterval(intervalID);
                User.getAuthenticatedUser(
                    function(value, responseHeaders){
                        $scope.user = value;
                        $scope.$emit('user:loggedIn', value);
                        var url = $scope.user.is_influencer ? "/boards" : "/intro";
                        $location.url(url);
                    },function(errorResponse){
                        alert(errorResponse.data.error);
                        window.location = "#/login";
                    }
                )
            }
        }, 500);
    }
});
