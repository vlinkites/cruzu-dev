'use strict';

var teamController = angular.module('cruzu.teamController', []);

teamController.controller('mainCtrl', function ($rootScope, $scope, $location, $anchorScroll, $route, $modal, $http, $q, User, Follow, influencers) {

    $scope.categories = ["All"];
    $scope.searchCategory = "All";

    $scope.influencers = influencers;

    $rootScope.user = User.getAuthenticatedUser(
        function (value, responseHeaders) {
            $rootScope.isAuthenticated = $rootScope.user.is_registered;

            // Pop up the initial help the first time (until they're registered
            if (!$rootScope.user.is_registered) {

                $modal.open({
                    templateUrl: 'modalIntro.html',
                    controller: modalCtrl,
                    resolve : {
                        influencer: function () {
                            return $scope.influencers[0];
                        }
                    }
                });
            }


        }, function (errorResponse) {
            $location.path('/login');
        }
    );

    $scope.openModal = function (item) {
        var index = $scope.influencers.indexOf(item);
        var modalInstance = $modal.open({
            templateUrl: 'partials/influencer_questions.html',
            controller: influencerQuestionCtrl,
            resolve: {
                influencer: function () {
                    var index = $scope.influencers.indexOf(item);
                    return $scope.influencers[index];
                },
                board: function ($q, User) {
                    var deferred = $q.defer();
                    var callback = function (result) {
                        deferred.resolve(result);
                    };

                    User.getBoard({id: $scope.influencers[index].id}, callback);
                    return deferred.promise;
                }
            }
        });
    };

    // TEMPORARY FOR BETA PERIOD; THEN REMOVE - WILL GENERATE NEXT OFFER
    $scope.sendNextOffer = function () {

        User.getNextOffer();
        alert('Sending request for next offer');
    };

    $scope.setCategory = function (category) {
        $scope.searchCategory = $scope.categories[category];
    };

    $scope.modalInfluencer = function () {
        $modal.open({
            templateUrl: 'modalInfluencer.html',
            controller: modalCtrl
        });
    };

});

var influencerQuestionCtrl = function ($scope, $rootScope, $location, $modalInstance, User, influencer, board) {

    $scope.board = board;

    $scope.getControlType = function (index) {
        return $scope.board.controls[index].type;
    };


    $scope.ok = function () {

        // console.log("*** "+JSON.stringify($scope.board,null,4));

        // Set return_values based on user's selected value (which is the display_value). If there is an option list for the question
        // then go through those options until we find the one that matches the user selection and grab return_value from there
        for (var i = 0; i < $scope.board.controls.length; i++) {
            if (["text", "textarea"].indexOf($scope.board.controls[i].type) != -1) {
                $scope.board.controls[i].answer.return_value = $scope.board.controls[i].answer.value;       // text inputs don't have return value, so create it
            }
            else if ($scope.board.controls[i].options) {
                // If it's a checkbox then we have to go through selectedOptions from the multiselect dropdown and convert it into
                // the "checked" boolean element inside of each option (sparse vs. non-sparse array integration)
                if ($scope.board.controls[i].type == "checkbox") {
                    for (var j = 0; j < $scope.board.controls[i].options.length; j++) {
                        $scope.board.controls[i].options[j].checked = false;                // assume it's not selected
                        var selectedOptionLength = $scope.board.controls[i].selectedOptions ?
                            $scope.board.controls[i].selectedOptions.length : 0;
                        for (var k = 0; k < selectedOptionLength; k++) {
                            if ($scope.board.controls[i].options[j].return_value == $scope.board.controls[i].selectedOptions[k].return_value)
                                $scope.board.controls[i].options[j].checked = true;
                        }
                    }
                }
                else {  // not a checkbox
                    console.log("$scope.board.controls[i]: " + JSON.stringify($scope.board.controls[i], null, 4));
                    for (var j = 0; j < $scope.board.controls[i].options.length; j++) {
                        if ($scope.board.controls[i].answer.value == $scope.board.controls[i].options[j].value) {
                            $scope.board.controls[i].answer.return_value = $scope.board.controls[i].options[j].return_value;
                        }
                    }
                }

            }
        }

        User.followUserWithBoard({influencer_id: influencer.id, board: $scope.board},
            function (value, responseHeaders) {
                $rootScope.user.board_answers = value;
                $modalInstance.close($scope.board.confirmation);
            }, function (errorResponse) {
                $modalInstance.close(errorResponse.data.error);
            });

    };

    $scope.cancel = function () {
        if (influencer.button) {
            influencer.button.title = influencer.title;
            influencer.button.state = influencer.title === 'FOLLOWING';
        }

        $modalInstance.dismiss('cancel');
    };

    // initialize questions
    var init = function () {

        // find relevant answers by filtering for influencer.id
        var board_answers = $scope.user.board_answers.filter(function (item) {
            return item.influencer_id == influencer.id
        });


        // simplify calls to nested properties and prevent exception on empty arrays.
        // Use *last* object in board_answers because changing answers currently appends instead of updating.
        if (board_answers[0])
            $scope.answers = board_answers[board_answers.length - 1].answers

        // console.log("scope.answers= "+JSON.stringify($scope.answers,null,4));

        // loop through board controls and assign the value stored in the user object as its answer.
        for (var i = 0; i < $scope.board.controls.length; i++) {

            // console.log("init: $scope.board.controls[i]= "+JSON.stringify($scope.board.controls[i],null,4));

            if ($scope.answers) {
                var answer = $scope.answers.filter(function (item) {
                    return item.id == $scope.board.controls[i].id
                })[0];
                if (answer)
                    $scope.board.controls[i].answer = answer;
                else
                    $scope.board.controls[i].answer = $scope.board.controls[i].options[0];

                // Special processing for checkboxes - convert answer strings into objects from checkbox control options
                // This will be saved in selectedOptions to be compatible with multiselect dropdown
                if ($scope.board.controls[i].type == "checkbox") {
                    $scope.board.controls[i].selectedOptions = [];
                    $scope.board.controls[i].options.forEach(function (option) {
                        if ($scope.board.controls[i].answer.value.indexOf(option.value) != -1)               // We found an option that matches a value we save
                            $scope.board.controls[i].selectedOptions.push(option);
                    });
                }

            } else if ($scope.board.controls[i].type == 'dropdown') {
                // when 'following' rather than 'updating' use first answer as default on dropdowns
                $scope.board.controls[i].answer = $scope.board.controls[i].options[0];
            }
        }
    };
    init();
};

var modalCtrl = function ($scope, $modalInstance, influencer) {

    $scope.influencer = influencer;


    $scope.close = function () {
        $modalInstance.close();
    };
};


var confirmationModalCtrl = function ($scope, $modalInstance, $timeout) {
    $scope.close = function () {
        $modalInstance.close();
    };

    $timeout(function () {
        $scope.close();
    }, 3000);
};


var generalQuestionModalCtrl = function ($scope, $modalInstance) {
    $scope.close = function () {
        $modalInstance.close();
    };
};




teamController.controller('influencerQuestionCtrl', influencerQuestionCtrl);

function showArrow($rootScope, $scope, index) {
    if ($scope.influencers[index].title == "FOLLOW") {
        $scope.influencers[index].title = "FOLLOWING";
        $scope.followingInfluencerCount++;
    }
    else {
        $scope.influencers[index].title = "FOLLOW";
        $scope.followingInfluencerCount--;
    }

    $scope.showArrow = $scope.followingInfluencerCount >= 1;
    // $scope.showArrow =  $scope.followingInfluencerCount >= 3 && !$rootScope.user.is_registered;
    console.log($scope.showArrow);

}
