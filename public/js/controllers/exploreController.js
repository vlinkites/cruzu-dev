var app = angular.module('cruzu.exploreController', []);

app.controller('exploreCtrl', function ($scope, $modal, $http, $q, Product, User, user, advisors) {

    $scope.user = user;
    $scope.advisors = advisors;

    $scope.next_product = function(advisor) {
        index = advisor.products.indexOf(advisor.current_products[0]);
        start = index + advisor.block.length;

        if (start >= advisor.products.length)
            start = 0;

        advisor.current_products = advisor.products.slice(start, start + advisor.block.length);
    }

    $scope.previous_product = function(advisor) {
        index = advisor.products.indexOf(advisor.current_products[0]);

        if (index == 0 && advisor.block.length == 1)
            index = advisor.products.length;
        else if (index == 0)
            index = advisor.products.length + 1;

        start = index - advisor.block.length;
        advisor.current_products = advisor.products.slice(start, index);
    }

    $scope.open = function(data) {
        if (typeof data.product == 'string') {
            data.product = Product.getProduct({id: data.product});
        }
        var productDescription = $modal.open({
            windowClass: 'product-detail-modal description',
            templateUrl: 'partials/product-detail.html',
            scope: $scope,
            controller: productDetailCtrl,
            resolve: {
                data: function() { return data }
            }
        });
    };

    $scope.open_question_modal = function(advisor) {
        var modalInstance = $modal.open({
            templateUrl: 'partials/influencer_questions.html',
            controller: influencerQuestionCtrl,
            resolve: { 
              influencer: function() { return advisor; },
              board: function ($q, User) {
                  var deferred = $q.defer();
                  var callback = function (result) {
                      deferred.resolve(result);
                  };

                  User.getBoard({id: advisor.id}, callback);
                  return deferred.promise;
              }
            }
        });
    };

    $scope.toggle_advisor = function(advisor) {
        $scope.advisor = advisor;
    }

    $scope.quicksearch = function(value) {
        return $http.get('/product/' + value + '/list_active').then(function(res) {
            var products = [];
            angular.forEach(res.data, function(item) {
                products.push(item);
            });
            return products;
        });
    };
});

var productDetailCtrl = function($scope, $modalInstance, $http, ActiveProduct, data) {
    $scope.advisor  = data.advisor;
    $scope.controls = data.controls;
    $scope.board    = data.board;
    $scope.user_product = data.product;

    var init_reviews = function() {
        $scope.user_product.comment = $scope.user_product.comment || $scope.board.default_section_short_note;
        $scope.user_product.review = $scope.user_product.review || $scope.product.data.meta_review;
    };

    ActiveProduct.getProduct({product_id: data.product.product_id || data.product, vintage: data.product.vintage}, function(value) {
        $scope.product  = value;

        if ($scope.board) {
          $scope.section  = $scope.board.sections.indexOf(data.section);

          angular.forEach($scope.board.sections[$scope.section].products, function(product) {
              if (product.product_id === $scope.product.product_id && product.vintage === $scope.product.vintage)
                  $scope.selected = true;
          });
       }

       if ($scope.user_product) {
           init_reviews();
       }
    });

    $scope.close = function() { 
        $modalInstance.dismiss('close');
    };

    $scope.add_to_list = function() {
        var product = {product_id: $scope.product.product_id, product_name: $scope.product.meta.name, vintage: $scope.product.vintage};
        $scope.board.sections[$scope.section].products.push(product);
        $scope.selected = true;
        $scope.user_product = product;
        init_reviews();
        $scope.save(true);
    };

    $scope.delete_from_list = function() {
        var index = $scope.board.sections[$scope.section].products.indexOf($scope.user_product);
        $scope.board.sections[$scope.section].products.splice(index, 1);
        $scope.save();
    };

    $scope.save = function(keep_open) {
        $http.post('/board/' + $scope.board._id, $scope.board).success(function(res) {
            if (!keep_open) {
                $scope.close();
            }
        });
    };
};


