var nconf = require('nconf'),
    path = require("path");

//
// Setup nconf to use (in-order):
//   1. Command-line arguments
//   2. Environment variables
//   3. A file located at 'path/to/config.json'
//

var currentEnv = process.env.NODE_ENV ? process.env.NODE_ENV : "development";
nconf.argv()
    .env()
    .file({ file: path.join('./config/',currentEnv+'.json') });

module.exports = nconf;