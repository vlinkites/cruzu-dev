var mongoose = require('mongoose'),
    ObjectId = require('mongoose').Types.ObjectId,
    Schema = mongoose.Schema,
    customError = require('../lib/custom_errors');

var questionSchema = new Schema({
    type: {type:String, required : true},
    text: {type:String, required : true},
    code: {type:String, required: true},
    answer: {
        data_type: String,
        sort_order: Number,
        default_text: String,
        control_type: String,
        option_list: [{return_value:String, display_value:String, image: String}],
        range: {
            high: Number,
            low: Number,
            step: Number
        }
    }
});

var questionModel = mongoose.model('Questions', questionSchema);

module.exports.addQuestion = function(data, callback){
    new questionModel(data).save(function(err, question){
        if(err)
            return callback(new customError.Database(err.toString()),null);

        callback(null,question);
    })
}

module.exports.getQuestionsByCondition = function(condition,limit, callback){
    questionModel.find(condition,{},{limit:limit,sort:{'text':1}},function(err,questions){
        if(err)
            return callback(new customError.Database(err.toString()),null);

        callback(null,questions);
    })
}