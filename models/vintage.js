var mongoose = require('mongoose'),
    Schema = mongoose.Schema;

module.exports.vintageSchema = new Schema({
        "vintage": Number,
        "data": {
            "lowest_price": Number,
            "average_price": Number,
            "list_price": Number,
            "minerality_level": Number,
            "facts": {
                "small_producer": Boolean,
                "new_producer": Boolean,
                "biodynamic": Boolean
            },
            "cruzu_score": Number,
            "qpr": Number,
            "max_score ": Number,
            "context": {
                "vintage_rating": Number
            },
            "comparison": {
                "previous_vintage": String
            },
            "oak": {
                "type": [String],
                "new_only": Boolean
            },
            "maturity": {
                "begin": Number,
                "end": Number
            },
            "cases": Number,
            "varietals": [String],
            "varietals_blend": [
                {
                    "varietal": String,
                    "percent": Number
                }
            ],
            "review_count": Number,
            "meta_review": String
        },
        "sources": [
            {
                "source_id": {type: Schema.Types.ObjectId, ref: 'Sources'},
                "source_name": String,         // Denormalized from Sources
                "qty_backordered": Number,
                "link": String,
                "qoh": Number,
                "actual_price": Number,
                "list_price" : Number,
                "last_updated": {type: Date, default: Date.now}
            }
        ],
        "reviews": [
            {
                // "reviewer_id": {type: Schema.Types.ObjectId, ref: 'Reviewers'},
                "reviewer" : String,
                "reviewer_name" : String,
                "score": Number,
                "score_str": String,
                "review_date": {type: Date, default: Date.now},
                "review_text": String
            }
        ],
        "sensory": {
            "fruit": [{name: String, count: Number}],
            "spice": [{name: String, count: Number}],
            "herb": [{name: String, count: Number}],
            "funk_earth": [{name: String, count: Number}],
            "floral": [{name: String, count: Number}],
            "body": [{name: String, count: Number}],
            "fault": [{name: String, count: Number}],
            "microbial": [{name: String, count: Number}],
            "mineral_inorganic": [{name: String, count: Number}],
            "oak": [{name: String, count: Number}],
            "appearance": [{name: String, count: Number}],
            "alcohol" : Number,
            "acid_score" : Number,
            "acid" : [String],
            "tannin_score" : Number,
            "tannin" : [String],
            "color" : String
        }
});



//var vintageModel = mongoose.model('Vintages', vintageSchema);
