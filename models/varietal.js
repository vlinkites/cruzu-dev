var mongoose = require('mongoose'),
    ObjectId = require('mongoose').Types.ObjectId,
    Schema = mongoose.Schema,
    customError = require('../lib/custom_errors');

var varietalSchema = new Schema({
    name: String,
    color: String
});

var varietalModel = mongoose.model('Varietals', varietalSchema, 'taxonomy_varietal');
module.exports.Varietals = varietalModel;


