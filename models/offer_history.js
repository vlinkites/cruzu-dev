var mongoose = require('mongoose'),
    ObjectId = require('mongoose').Types.ObjectId,
    Schema = mongoose.Schema,
    customError = require('../lib/custom_errors'),
    async = require('async'),
    productModel = require('../models/product'),
    venueModel = require('../models/venue'),
    reviewerModel = require('../models/reviewer'),
    _ = require('underscore')._;

mongoose.set('debug', true);


var offerSchema = new Schema({
    user_id: {type: Schema.Types.ObjectId, ref: 'Users'},
    products: [
        {
            product_id: {type: Schema.Types.ObjectId},
            vintage: Number,
            qty: Number,
            name: String,
            title: String,
            reasons_buy: [

            ],
            list_price: Number,
            discount_pct: Number,
            actual_price: Number,
            score: Number,
            other_scores: [
                {reviewer: String, score: Number}
            ],
            sensory_images: [String]
        }
    ],
    source: {
        source_id: {type: Schema.Types.ObjectId, ref: "Sources"},
        name: String,
        city_state: String,
        phone: String,
        url: String,
        email: String,
        location: []
    },
    totals: {
        subtotal: Number,
        sales_tax: Number,
        shipping: Number,
        total: Number
    },
    buy_link: String
});



var offerModel = mongoose.model('Offer_History', offerSchema, 'offer_history');


module.exports.getOneOffer = function (criteria, callback) {
    offerModel.find(criteria)   //criteria
        .sort({offer_date:-1})
        .limit(1)
        .exec(function (err, offers) {
            if (err)
                return callback(new customError.Database(err.toString()), null);
            var result = offers && offers.length > 0 ? offers[0].toObject() : null;
            callback(null, result);
        })
};



module.exports.saveClick = function (offerId, sourceId, callback) {
    offerModel.findOne({_id: offerId}).populate("product_id").exec(function (err, offer) {
        if (err)
            return callback(new customError.Database(err.toString()), null);

        // Only push response if we haven't already before
        var foundItem = _.find(offer.responded, function (obj) {
            return obj.toString() == sourceId;
        });
        if (!foundItem)
            offer.responded.push(sourceId);


        offer.save(function (err) {

            // We now need to return the link that the client should navigate to. To get this, we have to navigate into product_id
            // and then find the correct vintage and then the correct source within that vintage and get the link from there
            var foundVintage = _.find(offer.product_id.vintages, function (vintage) {
                return vintage.vintage == offer.vintage;
            });
            var foundSource = _.find(foundVintage.sources, function (source) {
                return source.source_id.toString() == sourceId;
            });
            if (foundSource)
                return callback(null, foundSource.link);
            else
                return callback(1, "www.cruzu.com");
        })
    })
};
