var mongoose = require('mongoose'),
    ObjectId = require('mongoose').Types.ObjectId,
    Schema = mongoose.Schema,
    customError = require('../lib/custom_errors');

var regionSchema = new Schema({
        country: String,
        region: String,
        regions: String
});

var regionModel = mongoose.model('Regions', regionSchema);
module.exports.Regions = regionModel;


