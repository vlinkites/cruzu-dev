var mongoose = require('mongoose'),
    ObjectId = require('mongoose').Types.ObjectId,
    Schema = mongoose.Schema,
    customError = require('../lib/custom_errors');

var reviewerSchema = new Schema({
    name: String,
    initials: String,
    importance: Number
});

var reviewerModel = mongoose.model('Reviewers', reviewerSchema);
module.exports.Reviewer = reviewerModel;

module.exports.addReviewer = function(data, callback){
    new reviewerModel(data).save(function(err, reviewer){
        if(err)
            return callback(new customError.Database(err.toString()),null);

        callback(null, reviewer);
    })
}
