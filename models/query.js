var mongoose = require('mongoose'),
    ObjectId = require('mongoose').Types.ObjectId,
    Schema = mongoose.Schema,
    _ = require('underscore')._;


var querySchema = new Schema({
    attribute: String,
    type: String,
    multi: Boolean,
    sort_code: Number,
    value: []
});


var queryModel = mongoose.model('Attributes', querySchema);
module.exports = queryModel;


module.exports.getAllQueryAttributesAndListValues = function (callback) {

    queryModel.find().sort("sort_code").lean().exec(function (err, attributes) {
        if (err)
            return callback(new customError.Database(err.toString()), null);

        // Flatten the values structure for each attribute if it exists so client doesn't have to do it

        attributes.forEach(function(attribute) {
            if (attribute.value instanceof Array)
                attribute.flat_list = expandAttributeValueList(attribute.value, []);
                attribute.flat_list = _.sortBy(attribute.flat_list, function(attrib) {return attrib; })
        });

        callback(null, attributes);

    })
};

// Flatten out recursive attribute list - note that this code contains an ugly hack
function expandAttributeValueList(ary, memo) {

    for (var i = 0; i < ary.length; i++) {
        if (typeof ary[i] === "string") {
            memo = memo.concat(ary[i]);
        }
        else {
            // Ugly hack below... for some reason the function sends a "0" as the key (certainly my problem) so I just strip
            // those out, but shouldn't have to have this following line. I'm sorry.
            var toAdd =  (_.keys(ary[i])[0] != "0") ? [_.keys(ary[i])[0]] : [];

            memo = (expandAttributeValueList(_.values(ary[i]).concat(toAdd), memo));
        }
    }
    return memo;
}


