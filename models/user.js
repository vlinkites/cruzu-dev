var mongoose = require('mongoose'),
    ObjectId = require('mongoose').Types.ObjectId,
    Schema = mongoose.Schema,
    customError = require('../lib/custom_errors'),
    async = require('async'),
    productModel = require('../models/product'),
    uuid = require('node-uuid'),
    venueModel = require('../models/venue'),
    reviewerModel = require('../models/reviewer');

var userSchema = new Schema({
    fb_id: {type:String, required : true},
    fb_access_token: {type:String, required : true},
    api_key: {type:String, default : uuid.v1()},
    name: String,
    email: String,
    address: {
        city: String,
        state_region: String,
        postal_code: String,
        country: String
    },
    short_bio: {type: String, default: ''},
    image: String,
    profile_answers: [],
    board_answers:[],
    is_influencer: {type: Boolean, default: false},
    in_development : {type: Boolean, default: false},
    is_influencer_action: {type: Boolean, default: false},
    has_influencer_questions : {type: Boolean, default: false},
    influencer_code : String,
    long_bio: {type: String, default: ''},
    category:  {type: String, default: 'Individual'},
    priority: Number,
    is_featured: {type: Boolean, default: false},
    followers: [{type: Schema.Types.ObjectId, ref:'Users'}],
    following: [{type: Schema.Types.ObjectId, ref:'Users'}],
    registrationDate: {type: Date, default: Date.now},
    is_registered: {type: Boolean, default: false},
    is_active : {type: Boolean, default:true},
    tags: {type: String, default: ''},
    hide_offer_msg : {type: Boolean, default:false},
    preferences : []
});

var userModel = mongoose.model('Users', userSchema);

module.exports.registerUser = function(data, callback){
    new userModel(data).save(function(err, user){
        if(err)
            return callback(new customError.Database(err.toString()),null);

        callback(null,user);
    })
}

module.exports.getUserByCondition = function(condition, callback){
    userModel.findOne(condition,function(err,user){
        if(err)
            return callback(new customError.Database(err.toString()),null);
        callback(null,user);
    })
}

module.exports.getUsersByCondition = function(condition, callback){
    userModel.find(condition,function(err,users){
        if(err)
            return callback(new customError.Database(err.toString()),null);

        callback(null,users);
    })
}

module.exports.updateUserFields = function(userId,fields,callback){
    userModel.update({_id:ObjectId(userId)},{$set:fields},function(err,effected,raw){
        if(err)
            return callback(new customError.Database(err.toString()),null);

        callback(null,{status:"updated"});
    })
}

module.exports.updateUserBoardAnswers = function(userId,condition,callback){
    userModel.update({_id:ObjectId(userId)},{$pull:condition},function(err,effected,raw){
        if(err)
            return callback(new customError.Database(err.toString()),null);

        callback(null,{status:"updated"});
    })
}

module.exports.followUser = function(userId, followId, arrayToUpdate, callback){
    var data = {};
    data[arrayToUpdate] = ObjectId(followId);
    userModel.update({_id:ObjectId(userId)},{$push:data},function(err,effected,raw){
        if(err)
            return callback(new customError.Database(err.toString()),null);

        callback(null,{status:"updated"});
    })
}

module.exports.unFollowUser = function(userId, followId, arrayToUpdate, callback){
    var data = {};
    data[arrayToUpdate] = ObjectId(followId);
    userModel.update(
        {_id:ObjectId(userId)},
        {$pull:data},
        function(err,numberEffected,raw){
            if (err)
                return callback(new customError.Database(err.toString()),null);

            callback(null, {status:"updated"});
        })
}