var mongoose = require('mongoose'),
    ObjectId = require('mongoose').Types.ObjectId,
    Schema = mongoose.Schema,
    customError = require('../lib/custom_errors');

var sectionSchema = new Schema({
    section_name : String,
    short_note : String,
    long_note : String,
    predicate : [],
    products : [{
        vintage: Number,
        product_name : String,
        product_id : Schema.Types.ObjectId,
        _id : Schema.Types.ObjectId,
        comment : String,
        review : String
    }]
});


var boardSchema = new Schema({
    name: String,
    description: String,
    image: String,
    agent_base_url : String,
    long_note : String,
    short_note : String,
    agent_alias: String,
    is_new_type : Boolean,
    agent_alias_bio : String,
    default_section_short_note : String,
    owner_id: {type: Schema.Types.ObjectId, ref:'Users'},
    confirmation: String,
    publish_state: {type: String, default:'published'},
    questions: [{type: Schema.Types.ObjectId, ref:'Questions'}],
    subject_line_pattern : String,
    reco_template : String,
    sections: [sectionSchema]
});

var boardModel = mongoose.model('Boards', boardSchema);
module.exports = boardModel;

module.exports.saveBoard = function(data, callback){
    new boardModel(data).save(function(err, board){
        if(err)
            return callback(new customError.Database(err.toString()),null);

        callback(null,board);
    })
};

module.exports.deleteBoard = function(data, callback){
    new boardModel(data).delete(function(err, board){
        if(err)
            return callback(new customError.Database(err.toString()),null);

        callback(null,board);
    })
};


module.exports.getBoard = function(condition, callback){
    boardModel
        .findOne(condition)
        .populate('owner_id questions')
        .exec(function(err,board){
            if(err)
                return callback(new customError.Database(err.toString()),null);

            callback(null,board);
        });
};



module.exports.getBoardsForAgent = function(ownerId, callback){
    boardModel
        .find({owner_id:ownerId})
        .populate('questions')
        .exec(function(err, boards){
        if(err)
            return callback(new customError.Database(err.toString()),null);

            callback(null,boards);
        });
};


module.exports.addProduct = function(ownerId, product, callback){
    boardModel.findOne({owner_id:ownerId}, function(err, board){
        if(err)
            return callback(new customError.Database(err.toString()),null);

        // Have to convert to ObjectID because we don't have the embedded product object modeled in Mongoose (MB 12/24/13)
        product.product_id = ObjectId(product.product_id);

        board.products.push(product);
        board.save(function(err){
            if(err)
                return callback(new customError.Database(err.toString()),null);

            callback(null,board);
        })
    })
}