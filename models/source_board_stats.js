var mongoose = require('mongoose'),
    ObjectId = require('mongoose').Types.ObjectId,
    Schema = mongoose.Schema,
    customError = require('../lib/custom_errors');

var sourceBoardStatsSchema = new Schema({
    count : Number,
    avg_score : Number,
    avg_price : Number,
    avg_qpr : Number,
    avg_multiple : Number,
    best_qpr : Number,
    best_multiple : Number,
    red_ratio : Number,
    white_ratio : Number,
    rose_ratio : Number,
    sparkling_ratio : Number,
    sweet_ratio : Number,
    sort_score : Number,
    source_id : {type: Schema.Types.ObjectId, ref:'Sources'},
    source_name : String,
    board_id : {type: Schema.Types.ObjectId, ref:'Boards'},
    board_name : String
});

var sourceBoardStatsModel = mongoose.model('SourceBoardStats', sourceBoardStatsSchema, 'source_board_stats');
module.exports = sourceBoardStatsModel;

