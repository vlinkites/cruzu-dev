var mongoose = require('mongoose'),
    ObjectId = require('mongoose').Types.ObjectId,
    Schema = mongoose.Schema;

var itemSchema = new Schema({
    "name" : String,
    "link_type" : String,
    "icon" : String,
    "link_code" : Schema.Types.ObjectId,
    "link_board" : Schema.Types.ObjectId
});
var blockSchema = new Schema({
    "header_type" : String,
    "title" : String,
    "subtitle" : String,
    "include_header_agent" : Boolean,
    "header_image" : String,
    "header_image_size" : String,
    "type" : String,
    "item_list" : [itemSchema],
    "agent" : {type: Schema.Types.ObjectId, ref: 'Users'},
    "board" : {type: Schema.Types.ObjectId, ref: 'Boards'},
    "item_size" : String,
    "item_orientation": String,
    "count": Number,
    "content": String,
    "image" : String,
    "selected_values": []
});

var pageSchema = new Schema({
    "title" : String,
    "image" : String,
    "page_code" : Schema.Types.ObjectId,
    "block_list" : [blockSchema],
    "trigger_beacon" : {
        "name": String,
        "uuid" : String,
        "major" : Number,
        "minor" : Number
    }
});

var AppSchema = new Schema({
    "target_venues" : [{type: Schema.Types.ObjectId, ref: 'Venues'}],
    "target_venue_type" : String,
    "name" : String,
    "owner_id" : [{type: Schema.Types.ObjectId, ref: 'Users'}],
    "logo" : String,
    "sort_order" : Number,
    "intro_content" : String,
    "app_deleted" : Boolean,
    "is_new_type" : Boolean,
    "pages" : [pageSchema]
});

var appModel = mongoose.model('Apps', AppSchema);
module.exports = appModel;


module.exports.getAppsForUser = function(user_id, callback){

    appModel.find({owner_id: user_id, app_deleted: {$ne:true}}, function(err,apps){
        if(err)
            return callback(new customError.Database(err.toString()),null);

        callback(null,apps);
    })

};

module.exports.upsertApp = function(app_id, content, callback) {

    // We have to delete _id as Mongoose won't allow us to save over
    delete content._id;

    appModel.findByIdAndUpdate(app_id, content, {upsert:true}, function(err, result) {
        if(err)
            return callback(new customError.Database(err.toString()),null);

        callback (null, result);
    });
};

module.exports.createApp = function(content, callback) {

    new appModel(content).save(function(err, result) {
        if(err)
            return callback(new customError.Database(err.toString()),null);

        callback (null, result);
    });
};

// Flag app as deleted, don't actually delete app in case of accident
module.exports.markAppAsDeleted = function(app_id, callback) {

    appModel.findByIdAndUpdate({ _id: app_id }, {app_deleted:true}, function (err, result ) {

        if(err)
            return callback(new customError.Database(err.toString()),null);

        callback (null, result);
    });
};


