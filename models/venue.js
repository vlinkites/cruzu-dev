var mongoose = require('mongoose'),
    ObjectId = require('mongoose').Types.ObjectId,
    Schema = mongoose.Schema;

var sectionSchema = new Schema({
    "name" : String,
    "section_id" : {type: Schema.Types.ObjectId},
    "beacon" : {
        "uuid" : String,
        "major" : Number,
        "minor" : Number
    }
});

var venueSchema = new Schema({
    "name" : String,
    "code" : String,
    "owner_id" : String,
    "brand_at_front" : Boolean,
    reader_schedule: Number,
    source_object_reader : String,
    url : String,
    state_region : String,
    ship_to_states : [String],
    allows_pickups : Boolean,
    promotion_types : [],
    shipping_rates : [],
    "image" : String,
    "image_large" : String,
    "type" : String,
    "location" : {type: [Number], index: '2d'},
    "city" : String,
    "library_items" : [String],
    "active" : Boolean,
    "disable" : Boolean,
    library_items : [String],
    "has_beacons" : Boolean,
    "price_string" : String,
    "trigger_beacons" : [{
        "name": String,
        "uuid" : String,
        "major" : Number,
        "minor" : Number
    }],
    "sections" : [sectionSchema]
});

module.exports = mongoose.model('Sources', venueSchema);

module.exports.addVenue = function(data, callback){
    new venueModel(data).save(function(err, source){
        if(err)
            return callback(new customError.Database(err.toString()),null);

        callback(null, source);
    })
}

