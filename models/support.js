var mongoose = require('mongoose'),
    ObjectId = require('mongoose').Types.ObjectId,
    Schema = mongoose.Schema,
    customError = require('../lib/custom_errors');

var supportSchema = new Schema({
    code: String,
    content: Schema.Types.Mixed
});

var supportModel = mongoose.model('Support', supportSchema, "support_data");
module.exports = supportModel;


