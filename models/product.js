var mongoose = require('mongoose'),
    ObjectId = require('mongoose').Types.ObjectId,
    Schema = mongoose.Schema,
    vintage = require('./vintage'),
    textSearch = require('mongoose-text-search'),
    customError = require('../lib/custom_errors');

var productSchema = new Schema({

    meta : {
        name: String,
        brand: String,
        region: String,
        designation: String,
        name_varietal: String,
        varietals: [],
        wine_type: String,
        processed_name: String,
        processed_brand: String
    },
    vintages: [vintage.vintageSchema]
});

productSchema.plugin(textSearch);

var productModel = mongoose.model('Products', productSchema);
module.exports.Product = productModel;

// This is the smaller "active" product list
var activeProductSchema = new Schema({
    meta: {
        name : String,
        product_id : Schema.Types.ObjectId
    }
});

var activeProductModel = mongoose.model('ActiveProducts', activeProductSchema, 'products_denormalized');
module.exports.ActiveProduct = activeProductModel;

module.exports.searchActiveProducts = function(str, vintage, callback){
    var srch = new RegExp(str,"i");
    vintage = vintage || {$lte:2015};       // How do we say "any value" including null???
    activeProductModel.find({"meta.name":srch, vintage:vintage})
        .select("product_id vintage meta")
        .lean()
        .limit(20)
        .exec(function (err, output) {
        if (err)
            return callback(new customError.Database(err.toString()), null);

        callback(null, output);
    })
};

// Return a full Active product for id
module.exports.getSingleActiveProduct = function(id, callback){
    activeProductModel.findOne({_id : id}, function(err, product){
        if (err)
            return callback(new customError.Database(err.toString()), null);

        callback(null, product);
    })
};

// Return a full Active product for id
module.exports.getSingleActiveProductFromProductID = function(id, vintage, callback){

    // If vintage has a value then get int value of vintage, otherwise assume it's "NV"
    vintage = (vintage && vintage.indexOf("null")==-1) ? parseInt(vintage) : null;
    activeProductModel.findOne({product_id : id, vintage: vintage}, function(err, product){
        if (err)
            return callback(new customError.Database(err.toString()), null);

        callback(null, product);
    })
};




module.exports.addProduct = function (data, callback) {
    new productModel(data).save(function (err, product) {
        if (err)
            return callback(new customError.Database(err.toString()), null);

        callback(null, product);
    })
};

module.exports.searchProducts = function(criteria, callback){
    productModel.textSearch(criteria, {filter:{'vintages': {$not: {$size: 0}}}}, function (err, output) {
        if (err)
            return callback(new customError.Database(err.toString()), null);

        callback(null, output);
    })
};

module.exports.getProduct = function(criteria, callback){
    productModel.findOne(criteria, function(err, product){
        if (err)
            return callback(new customError.Database(err.toString()), null);

        callback(null, product);
    })
};




